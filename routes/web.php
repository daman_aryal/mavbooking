<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get("welcome", function()
{
	return view('welcome');
});

//Auth::routes();

Route::get('/home', 'HomeController@index');

Route::group(['middleware'=>'web'], function()
{
	Route::group(['middleware'=>'auth'], function(){
		
		
			Route::get('dashboard', function (){
				$tot_agents = \App\User::where('user_type',0)->count();
				$flights = \App\Flight::all()->count();
				$tot_booking=\App\Ticket::all()->count();
				$collections=\App\MasterBill::all();
				$currency=new App\AgentDetail;
				$tot_collection_usd=0;
				$tot_collection_npr=0;
				foreach($collections as $collection)
				{
					if(($currency->getCurrency($collection->agent_id))=='USD'){
						$tot_collection_usd+=$collection->amount+$collection->tax_amount;
					}
					else{
						$tot_collection_npr+=$collection->amount+$collection->tax_amount;
					}
				}
				//dd($tot_collection_npr);
				$today_flights=\App\Scheduler::where('date',date('Y-m-d'))->select('flight_id','date','arrive_time','depart_time')->distinct('depart_time','arrive_time')->get();

				$tickets=\App\Ticket::orderBy('created_at','desc')->paginate(10);
				$model_ticket=new App\Ticket;

			
				return view('backend.dashboard')->withTot_agents($tot_agents)->withFlights($flights)->withTot_booking($tot_booking)->withTot_collection_usd($tot_collection_usd)->withTot_collection_npr($tot_collection_npr)->withToday_flights($today_flights)->withTickets($tickets)->withModel_ticket($model_ticket);
			});


			Route::group(['prefix'=>'flight'], function()
			{
				Route::get('/', 'FlightController@index');
				Route::get('/dt', 'FlightController@datatable');
				Route::get('/create', 'FlightController@create');
				Route::get('/delete/{id}', 'FlightController@destroy');
				Route::get('/edit/{id}', 'FlightController@edit');
				Route::post('/store', 'FlightController@store');
				Route::post('/update/{id}', 'FlightController@update');
			});

			Route::group(['prefix'=>'schedule'], function()
			{
				Route::get('/', 'ScheduleController@index');
				Route::get('/create', 'ScheduleController@create');
				Route::get('/delete/{id}', 'ScheduleController@destroy');
				Route::get('/edit/{id}', 'ScheduleController@edit');
				Route::post('/store', 'ScheduleController@store');
				Route::post('/update/{id}', 'ScheduleController@update');
			});




			Route::group(['prefix'=>'scheduler'], function()
			{
				Route::get('/', 'SchedulerController@index');
				Route::post('/loader', 'SchedulerController@loader');
				Route::get('/create', 'SchedulerController@create');
				Route::get('/edit/{id}', 'SchedulerController@edit');
				Route::get('/show/{id}', 'SchedulerController@show');
				Route::post('/store', 'SchedulerController@store');
				Route::post('/update/{id}', 'SchedulerController@update');
			});


			//Route::get('create',['uses'=>'HomeController@create','middleware' => ['permission:edit-user']]);
			//, 'middleware' => 'permission:user-create'
			Route::group(['prefix'=>'admin'], function()
			{
				Route::get('/', 'UserController@index');
				Route::get('/dt', 'UserController@datatable');
				Route::get('/create', 'UserController@create');
				Route::get('/edit/{id}', 'UserController@edit');
				Route::post('/store', 'UserController@store');
				Route::get('show/{id}', 'UserController@show');
				Route::post('/update/{id}', 'UserController@update');
				Route::get('/delete/{id}', 'UserController@destroy');
			});

			Route::group(['prefix'=>'agent'], function()
			{
				Route::get('/', 'AgentController@index');
				Route::get('/dt', 'AgentController@datatable');
				Route::get('/create', 'AgentController@create');
				Route::get('show/{id}', 'AgentController@show');
				Route::get('/edit/{id}', 'AgentController@edit');
				Route::post('/store', 'AgentController@store');
				Route::post('/update/{id}', 'AgentController@update');
				Route::get('/delete/{id}', 'AgentController@destroy');

				//agen account

				Route::get('ac/', 'AgentController@ac_index');
				Route::get('ac/create/{id}', 'AgentController@ac_create');
				Route::get('ac/edit/{id}', 'AgentController@ac_edit');
				Route::post('ac/store', 'AgentController@ac_store');
				Route::post('ac/update/{id}', 'AgentController@ac_update');


				/**
				 * front end for agent
				 */
				//Route::get('/dashboard', 'AgentController@search');
				/*Route::post('/search', 'AgentController@search');*/
			});

			Route::group(['prefix'=>'agent_ac'],function(){
				Route::get('/','AgentController@ac_summary');
				Route::get('/agentpnrs/{agent_id}',['uses'=>'AgentController@agent_all_pnrs','as'=>'agent_ac.agentallpnrs']);
				Route::get('/pnrtickets/{master_id}',['uses'=>'AgentController@pnr_all_tickets','as'=>'agent_ac.pnralltickets']);
			});

			Route::group(['prefix'=>'role'], function()
			{
				Route::get('/', 'RoleController@index');
				Route::get('/create', 'RoleController@create');
				Route::get('show/{id}', 'RoleController@show');
				Route::get('/edit/{id}', 'RoleController@edit');
				Route::post('/store', 'RoleController@store');
				Route::post('/update/{id}', 'RoleController@update');
				Route::get('/delete/{id}', 'RoleController@destroy');
			});

			Route::group(['prefix'=>'city'], function()
			{
				Route::get('/create',['uses'=>'CityController@create','as'=>'city.create']);
				Route::post('/',['uses'=>'CityController@store','as'=>'city.store']);
				Route::get('/',['uses'=>'CityController@index','as'=>'city.index']);
				Route::delete('/{id}',['uses'=>'CityController@destroy','as'=>'city.destroy']);
				Route::get('/{id}/edit',['uses'=>'CityController@edit','as'=>'city.edit']);
				Route::put('/{id}',['uses'=>'CityController@update','as'=>'city.update']);
			});

			Route::group(['prefix'=>'cabin'], function()
			{
				Route::get('/create',['uses'=>'CabinController@create','as'=>'cabin.create']);
				Route::post('/',['uses'=>'CabinController@store','as'=>'cabin.store']);
				Route::get('/',['uses'=>'CabinController@index','as'=>'cabin.index']);
				Route::delete('/{id}',['uses'=>'CabinController@destroy','as'=>'cabin.destroy']);
				Route::get('/{id}/edit',['uses'=>'CabinController@edit','as'=>'cabin.edit']);
				Route::put('/{id}',['uses'=>'CabinController@update','as'=>'cabin.update']);
			});

			Route::group(['prefix'=>'rbd'], function()
			{
				Route::get('/create',['uses'=>'RbdController@create','as'=>'rbd.create']);
				Route::post('/',['uses'=>'RbdController@store','as'=>'rbd.store']);
				Route::get('/',['uses'=>'RbdController@index','as'=>'rbd.index']);
				Route::delete('/{id}',['uses'=>'RbdController@destroy','as'=>'rbd.destroy']);
				Route::get('/{id}/edit',['uses'=>'RbdController@edit','as'=>'rbd.edit']);
				Route::put('/{id}',['uses'=>'RbdController@update','as'=>'rbd.update']);
			});

			Route::group(['prefix'=>'tax'], function()
			{
				Route::get('/create',['uses'=>'TaxController@create','as'=>'tax.create']);
				Route::post('/',['uses'=>'TaxController@store','as'=>'tax.store']);
				Route::get('/',['uses'=>'TaxController@index','as'=>'tax.index']);
				Route::delete('/{id}',['uses'=>'TaxController@destroy','as'=>'tax.destroy']);
				Route::get('/{id}/edit',['uses'=>'TaxController@edit','as'=>'tax.edit']);
				Route::put('/{id}',['uses'=>'TaxController@update','as'=>'tax.update']);
				Route::get('/gettax',['uses'=>'TaxController@getTax','as'=>'tax.getTax']);
			});

			Route::group(['prefix'=>'seat'], function()
			{
				Route::get('/create',['uses'=>'SeatController@create','as'=>'seat.create']);
				Route::post('/',['uses'=>'SeatController@store','as'=>'seat.store']);
				Route::get('/',['uses'=>'SeatController@index','as'=>'seat.index']);
				Route::delete('/{id}',['uses'=>'SeatController@destroy','as'=>'seat.destroy']);
				Route::get('/{id}/edit',['uses'=>'SeatController@edit','as'=>'seat.edit']);
				Route::put('/{id}',['uses'=>'SeatController@update','as'=>'seat.update']);
			});

			Route::group(['prefix'=>'meal'], function()
			{
				Route::get('/create',['uses'=>'MealController@create','as'=>'meal.create']);
				Route::post('/',['uses'=>'MealController@store','as'=>'meal.store']);
				Route::get('/',['uses'=>'MealController@index','as'=>'meal.index']);
				Route::delete('/{id}',['uses'=>'MealController@destroy','as'=>'meal.destroy']);
				Route::get('/{id}/edit',['uses'=>'MealController@edit','as'=>'meal.edit']);
				Route::put('/{id}',['uses'=>'MealController@update','as'=>'meal.update']);
			});

	      	Route::group(['prefix'=>'fare'], function()
		    {
		        Route::post('/entry/{id}',array('as'=>'fare.update','uses'=>'FareController@update'));
		        Route::get('/select',array('as'=>'fare.check','uses'=>'FareController@check'));
		        Route::post('/select',array('as'=>'fare.check','uses'=>'FareController@check'));
		        Route::post('/create',array('as'=>'fare.create','uses'=>'FareController@store'));
		        Route::get('/',array('as'=>'fare.index','uses'=>'FareController@index'));
		    });
	      	


		    Route::group(['prefix'=>'reservation'], function()
		    {
		        Route::get('/',array('as'=>'reservation.index','uses'=>'ReservationController@index'));
		        Route::get('/no/show/{pnr_no}', 'ReservationController@noShow');
		        Route::get('/cancle/{pnr_no}', 'ReservationController@cancel');
				Route::post('check/availabiltiy', 'ReservationController@checkAvailability');
				Route::post('datechange/reserve', 'ReservationController@dateChangeReserve');
			});
			//Route::resource('/city', 'CityController');


		/**
		 * Only for
		 * Agen front End Design
		 */

		
		Route::group(['prefix'=>'reservation'], function()
		{
			/**
			 * Agent frontend controller
			 * page after search page,
			 * where passanger detail retrived
			 */
			Route::post('/search', 'ReservationController@search');
			/**
			 * save passanger detail
			 * Full information of one passanger and
			 * some basic information about others
			 */
			
			Route::post('/reserve', 'ReservationController@reserve');
			/**
			 * refund
			 */
			Route::post('/refund', 'ReservationController@refund');
			/**
			 * redirect after 
			 * saveing passenger
			 * inforamtion
			 */
			Route::get('/reserve/complete', 'ReservationController@reserve_complete');
			Route::get('/ticket/print/{passenger_id}', 'ReservationController@ticket_print');

			Route::get('/bill', function(){
				return view('frontend.bill');
			});

		});

        Route::get('/',array('as'=>'reservation.index','uses'=>'ReservationController@index'));

        Route::get('/no/show/{pnr_no}', 'ReservationController@noShow');

        Route::get('/datechange/{ticket_id}',array('as'=>'reservation.dateChange','uses'=>'ReservationController@dateChange'));
        Route::post('/updatedate/{pnr}',array('as'=>'reservation.update_date','uses'=>'ReservationController@dateUpdate'));
		
		//Route::resource('/city', 'CityController');
		Route::get('/agent/dashboard', function () {

			$cities = \App\Scheduler::distinct()->get();
			$cabin = \App\Cabin::all();

			$cities = \App\Scheduler::select('source','destination')->distinct()->get();
		return view('frontend.index',compact('cabin'))->withCities($cities);

		});
		
	});
});




Route::group(['prefix' => 'api', 'namespace' => 'Api'], function () {
  Route::get('/agent/city', ['as'=> 'api.city', 'uses' => 'CityController@getCitySource']);
  Route::get('/agent/cityDestination', ['as'=> 'api.city', 'uses' => 'CityController@getCityDestination']);

});

/**
 * anauthorized pages
 */
Route::get('unauthorized', function ()
{
	return view('errors.403');
});
