<?php

namespace App\BillingModel;

use App\BillDetail;
use App\MasterBill;
use App\MasterRefund;
use App\RefundDetail;
use App\Ticket;
use Auth;
use DB;

class BillingModel{

	public function newTicket($pnr_no, $passenger_id, $scheduler_id, $agent_id, $status=null)
	{
		$tiket_id = 0;
        /**
         * find the latest ticket no and add by 1 so that your can
         * increment ticket id
         * @var [stype]
         */
        $status_value = 0;
        
        if($status !== null){
        	$status_value = $status;
        }
		
		$max_id = DB::table('tickets')->max('id');
        $ticket = new Ticket();
        $ticket->pnr = $pnr_no;
        $ticket->ticket_no = 'ticket@prefix'.$max_id;   
        $ticket->status = $status_value;
        $ticket->passenger_id = $passenger_id;
        $ticket->agent_id = $agent_id;
        $ticket->save();
        /**
         * maintain the ticket and scheduler seprate table
         */
        if(is_string($scheduler_id)){
        	DB::table('ticket_scheduler')->insert([
                'ticket_id' => $ticket->id,
                'scheduler_id' => $scheduler_id
            ]);
        }else{
        	foreach ($scheduler_id as $key => $value) {
	            DB::table('ticket_scheduler')->insert([
	                'ticket_id' => $ticket->id,
	                'scheduler_id' => $value
	            ]);
	        }	
        }
        return $ticket;
	}

	public function masterBill($pnr_no, $amount, $tax_amount, $agent_id)
	{
		$master_bill = new MasterBill();
        /**
         * multiple price to every passenger for total bill amoun
         * @var [type]
         */
        $master_bill->amount = $amount;
        $master_bill->tax_amount = $tax_amount;
        $master_bill->pnr = $pnr_no;
        $master_bill->agent_id = $agent_id;
        $master_bill->save();

        return $master_bill;
	}

	public function billDetail($master_id, $amount, $tax_amount, $ticket_id, $passenger_id, $agent_id)
	{
		$tax = DB::table('taxes')
                ->select('id', 'local_price', 'tax_head','local_price', 'usd_price')
                ->get();
        /**bill detail for each bill*/
	    $bill_detail = new BillDetail();
        /**
         * if bill is refunded, $penalty will not be null,
         * so make a bill with penalty
         */
	    $bill_detail->master_bill_id = $master_id; 
	    $bill_detail->amount = $amount;
	    $bill_detail->tax_amount = $tax_amount;
	    $bill_detail->refund = 0;
	    $bill_detail->status = 0;
        $bill_detail->agent_id = $agent_id;
	    $bill_detail->ticket_id = $ticket_id;
	    $bill_detail->passenger_id = $passenger_id;
	    $bill_detail->remarks = "hello";
	    $bill_detail->save();

	    //add tax for every bill
        foreach ($tax as $key => $value) {
        $tax_detail_pivot = DB::table('tax_price_pivot')->insert([
                'tax_id' => $value->id,
                'tax_head' => $value->tax_head,
                'tax_percentage' => '25',
                'amount' => '200',
                'bill_detail_id' => $bill_detail->id,
                'passenger_id' => $passenger_id
            ]);
        }
    }

    public function refund($noshow, $masterBillId_asPerPnr, $master_bill_amount, $agent_id){
        /**
         * 1. fetch amount of particular bill and store in $noshow
         * 2. total amount from $noshow and get as total sum [$total_amt]
         * 2. fetch tax same as well [$total_tax]
         */
        $total_amt = 0;
        $total_tax = 0;
        
        foreach($noshow as $ns){
            $total_amt = $total_amt + $ns->amount; 
            $total_tax = $total_tax + $ns->tax_amount; 
        }
        
        /**
         * make master refund refrence from master bill id
         * and total amount [$total_amt] and tax amount [$total_tax]
         * @var MasterRefund
         */
        $master_refund = new MasterRefund();
        $master_refund->master_bill_id = $masterBillId_asPerPnr;
        $master_refund->amount = $total_amt;
        $master_refund->tax_amount = $total_tax;
        $master_refund->agent_id = $agent_id;
        $master_refund->save();

        /**
         * create master bill for refunded bill
         * which will include penalty excluding tax
         * $pnr no will be as generated randomly [str_random(8)]
         * @var [type]
         */
        $pnr_no = str_random(8);   
        $master_bill_id = $this->masterBill($pnr_no, $master_bill_amount, $agent_id);
        

        $master_bill = new MasterBill();
        $master_bill->amount = $master_bill_amount; 
        $master_bill->tax_amount = 0;
        $master_bill->pnr = $pnr_no;
        $master_bill->save();

        /**
         * 1. $nosow contains every bill detail
         * 2. refund detail will be created as per $noshow data
         */
        foreach($noshow as $ns){
            $refund_detail = new RefundDetail();
            $refund_detail->master_refund_id = $master_refund->id;
            $refund_detail->amount = $ns->amount;
            $refund_detail->tax_amount = $ns->tax_amount;
            $refund_detail->passenger_id = $ns->passenger_id;
            $refund_detail->agent_id = $agent_id;
            $refund_detail->save();
            

            //create a bill detail with penalty    
            $this->billDetail($master_bill_id->id, $master_bill_amount,0,$ns->ticket_id, $ns->passenger_id, $agent_id);
        }
    }

}   