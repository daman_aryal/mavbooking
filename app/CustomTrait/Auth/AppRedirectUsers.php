<?php

namespace App\CustomTrait\Auth;
use Auth;

trait AppRedirectUsers
{
    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function redirectPath()
    {
    	if(Auth::user()->user_type == 0){
    		$redirect = 'agent/dashboard';
    	}else{
    		$redirect = 'dashboard';
    	}
        
        return property_exists($this, 'redirectTo') ? $this->redirectTo : $redirect;
    
    }
}
