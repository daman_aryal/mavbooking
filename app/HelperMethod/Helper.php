<?php

namespace App\HelperMethod;

class Helper{
    /**
     * define status
     */
    function status($status){
        if($status == 0){
            $current_status = "Cancel";
        }elseif($status == 1){
            $current_status = "No show";
        }elseif($status == 3){
            $current_status == "Date Changed";
        }else{
            $current_status = "Normal";    
        }
        return $current_status;
    }
}
