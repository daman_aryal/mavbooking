<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminType
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->user_type !== 1){
            return abort(403);
        }
        return $next($request);
    }
}
