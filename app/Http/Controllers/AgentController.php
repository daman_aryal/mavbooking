<?php

namespace App\Http\Controllers;

use App\AgentAc;
use App\AgentDetail;
use App\BillDetail;
use App\Cabin;
use App\City;
use App\MasterBill;
use App\Passenger;
use App\Rbd;
use App\Scheduler;
use App\Tax;
use App\Ticket;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Yajra\Datatables\Datatables;

class AgentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('backend.agent.index');
    }


    public function datatable()
    {
       $user = DB::table('users')
            ->leftJoin('agent_details', 'users.id', '=', 'agent_details.user_id')
            ->where('user_type', '0')
            ->select('users.id', 'users.name', 'users.email', 'agent_details.org', 'agent_details.billing_currency')
            ->get();
       return Datatables::of($user)
            ->addColumn('action', function ($user)
            {
                $buttons = '<a href="agent/ac/create/'.$user->id.'" class="btn btn-success"><i class="fa fa-money"></i></a>
                <a type="button" href="'.url('agent/edit')."/".$user->id.'" class="editP btn btn-info"><i class="fa fa-edit"></i></a>
                <a href="agent/show/'.$user->id.'" class="btn btn-success"><i class="fa fa-eye"></i></a>
                <a type="button" href="'.url('agent/delete')."/".$user->id.'" class="editP btn btn-danger"><i class="fa fa-trash"></i></a>';
                return $buttons;
            })->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.agent.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'password_confirm' => 'required|same:password',
            'address' => 'required',
            'contact' => 'required | regex : /^[0-9-+]+$/',
            'contact_person' => 'required | regex : /^[a-zA-Z ]+$/',
            'phone_no' => 'required | regex : /^[0-9-+]+$/',
            'mobile_no' => 'required | regex : /^[0-9-+]+$/',
            'billing_currency' => 'required'
        ],
        [
            'name.regex' => 'Name only should contain letter and space'
        ]);


        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->address = $request->address;
        $user->contact = $request->contact;
        $user->password = Hash::make($request->password);
        $user->created_by ='1';
        $user->active = '0';
        $user->first_login = '0';
        $user->user_type = '0';
        $user->save();


        $detail =  new AgentDetail();
        $detail->user_id = $user->id;
        $detail->contact_person = $request->contact_person;
        $detail->phone_no = $request->phone_no;
        $detail->mobile_no = $request->mobile_no;
        $detail->org = $request->org;
        $detail->billing_currency = $request->billing_currency;
        $detail->save();
        return back()->with('success','User created successfully');
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $agent = DB::table('users')
            ->leftJoin('agent_details', 'users.id', '=', 'agent_details.user_id')
            ->where('user_id', $id)
            ->select('users.id', 'users.name', 'users.email', 'agent_details.org', 'agent_details.phone_no','agent_details.billing_currency', 'agent_details.mobile_no')
            ->first();
        return view('backend.agent.show',compact('agent'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_id = $id;
        $agent = DB::table('users')
                ->leftJoin('agent_details', 'users.id', '=', 'agent_details.user_id')
                ->where('users.id', $user_id)
                ->select('users.*','agent_details.*')
                ->first();

        return view('backend.agent.edit',compact('agent'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'password_confirm' => 'same:password',
            'address' => 'required',
            'contact' => 'required | regex : /^[0-9-+]+$/',
            'contact_person' => 'required | regex : /^[a-zA-Z ]+$/',
            'phone_no' => 'required | regex : /^[0-9-+]+$/',
            'mobile_no' => 'required | regex : /^[0-9-+]+$/',
            'billing_currency' => 'required'
        ],

        [
            'name.regex' => 'Name only should contain letter and space'
        ]);


        /**
         * email should not be same to other while updateing
         * but could be same to same user
         */


        $input = $request->all();

        $email_exit = User::where('email', $request->get('email'))
                            ->where('id', '!=', $id)
                            ->exists();
        if($email_exit){
            return back()->with(['email_exit'=>'Email already exit']);
        }

        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->address = $request->address;
        $user->contact = $request->contact;
        $user->created_by ='1';
        $user->active = '0';
        $user->first_login = '0';
        $user->user_type = '0';
        $user->save();


        $detail =  AgentDetail::where('user_id', $id)->first();
        $detail->user_id = $user->id;
        $detail->contact_person = $request->contact_person;
        $detail->phone_no = $request->phone_no;
        $detail->mobile_no = $request->mobile_no;
        $detail->org = $request->org;
        $detail->billing_currency = $request->billing_currency;
        $detail->save();

        return redirect('agent/')->with('success','Agent registration updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect('agent/')->with('success','User deleted successfully');
    }
    /**
     *
     * +++++++++++++++++++++++++ Agent Account ++++++++++++++++++++++++
     */
    public function ac_create(Request $request, $id)
    {
        $debit_credit = AgentAc::where('user_id', $id)->get();
        $agent=User::where('id',$id)->first();
        $billCurrency=AgentDetail::where('user_id',$id)->first();
        $total = 0;
        /**
         * total amount
         */
        foreach($debit_credit as $d){
            $total = $total + $d->debit - $d->credit;
        }

        $agent_balance = AgentAc::where('user_id', $id)->orderBy('id','desc')->paginate(5);
        return view('backend.agent_ac.create', compact('id', 'agent_balance','agent','billCurrency','total'));
    }


    public function ac_store(Request $request)
    {
        $this->validate($request, [
            'debit' => 'required|numeric|min:1'
        ]);

        $agentAc = new AgentAc();
        $agentAc->user_id = $request->user_id;
        $agentAc->credit = $request->credit;
        $agentAc->debit = '0';
        $agentAc->save();

        return back()->with('success','Balance added successfully');
    }

    public function ac_summary(){
         $ac_agents= AgentDetail::orderBy('id')->get();
         $agent_detail=new AgentDetail;
        return view('backend.agent_summary.index')->withAc_agents($ac_agents)->withAgent_detail($agent_detail);
    }

    public function agent_all_pnrs($agent_id){
        $agent_pnrs=MasterBill::where('agent_id',$agent_id)->get();
        $currency=new AgentDetail();
        $agent_name=MasterBill::where('agent_id',$agent_id)->select('agent_id')->first();
        $agent_detail=new AgentDetail;
        //dd($agent_pnrs);
        //dd($agent_name);
        // $agent_detail=new AgentDetail;
        return view('backend.agent_summary.agent_pnr')->withAgent_pnrs($agent_pnrs)->withAgent_detail($agent_detail)->withAgent_name($agent_name)->withCurrency($currency);
    }

    public function pnr_all_tickets($master_id){
         $pnr_tickets=BillDetail::where('master_bill_id',$master_id)->get();
         $passenger_name=new BillDetail();
         $currency=new AgentDetail();
         return view('backend.agent_summary.pnr_tickets')->withPnr_tickets($pnr_tickets)->withPassenger_name($passenger_name)->withCurrency($currency);
    }
}

