<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Scheduler;

class CityController extends Controller
{
    public function getCitySource(Request $req){
      $query = $req->get('city_name');
      $city = Scheduler::select('source')
                    ->where('source', 'like', "%$query%")
                    ->get();

      return response()->json($city);
    }

    public function getCityDestination(Request $req){
      $query = $req->get('city_name');
      $city = Scheduler::select('destination')
                    ->where('destination', 'like', "%$query%")
                    ->get();

      return response()->json($city);
    }
}
