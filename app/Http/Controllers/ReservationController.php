<?php

namespace App\Http\Controllers;

use App\AgentDetail;
use App\BillDetail;
use App\BillingModel\BillingModel;
use App\Cabin;
use App\City;
use App\MasterBill;
use App\MasterRefund;
use App\Passenger;
use App\Rbd;
use App\RefundDetail;
use App\Scheduler;
use App\Ticket;
use Auth;
use DB;
use Illuminate\Http\Request;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Ticket $ticket)
    {
        /**
         * 1. group ticket id by its pnr
         * 2. fetch maximum from this grouped pnr [we just need to display one pnr]
         * 3. store max pnr to $ticket[] array
         * @var [type]
         */
        $tickets = $ticket->ticket()->groupBy('pnr')->all();
        $ticket = [];
        foreach($tickets as $t){
            $ticket[] = $t->max();
        }
        return view('backend.reservation.index', compact('ticket'));
    }

    public function dateChange($pnr_no, Ticket $ticket, AgentDetail $agentDetail)
    {   

        $i = 1;
        /**
         * $ticket->as_pnr($pnr_no) function is defined in Ticket model
         * it get all tickets with status 0 where pnr no is $pnr_no
         * @var [type]
         */
        $tickets_as_pnr = $ticket->as_pnr($pnr_no);
        /**
         * fetch any ticket from the group of
         * pnr no
         * @var [type]
         */
        if($tickets_as_pnr->count() > 0){
            $ticket_first = $tickets_as_pnr->first();
            $billing_currency = $agentDetail->billing_currency($ticket_first->id);
            $scheduler_in_ticket = $ticket_first->scheduler_ticket_pivot($ticket_first->id);
            /**
             * if there is tickets with status 0 compact with return view
             */
            return view('backend.reservation.datechange', compact('billing_currency', 'scheduler_in_ticket', 'tickets_as_pnr','ticket_first', 'i', 'pnr_no'));     
        }
        return redirect()->back()->with(['msg'=>'No data found !']);
    }

    /*------date change option bhitra ko search------*/
    public function checkAvailability(Request $request){

        if($request->billing_currency == "USD"){
            $price_field = 'price_usd_oneway';
        }else{
            $price_field = 'price_local_oneway';
        }

        $scheduler = Scheduler::where('source',$request->source)
                     ->where('destination',$request->destination)
                     ->where('date',$request->date_change)
                     ->where('cabin_id',$request->cabin)
                     ->where('flight_id', $request->flight_id)
                     ->first();
        
        if(count($scheduler) > 0){
            return response()->json([
                'scheduler_id'=>$scheduler->id,
                'price' => $scheduler->$price_field
            ]);    
        }else{
            return response()->json(count($scheduler));    
        }
    }

    public function dateChangeReserve(Request $request, BillingModel $billingModel, MasterBill $masterBill){
        DB::transaction(function() use($request, &$billingModel, $masterBill)
        {
            $pnr_no = str_random(8);
            //new bill
            /**
             * get master bill id as per pnr no
             * one pnr no equal to one master bill
             */
            $masterBillId_asPerPnr = $masterBill->id_as_pnr($request->pnr_no);
            /**
             * 1. fetch amount of particular bill and store in $noshow
             * 2. total amount from $noshow and get as total sum [$total_amt]
             * 2. fetch tax same as well [$total_tax]
             */
            $total_amt = 0;
            $total_tax = 0;

            $noshow = DB::table('bill_details')->select('amount', 'tax_amount', 'passenger_id', 'ticket_id','agent_id')
                                    ->where('master_bill_id', $masterBillId_asPerPnr)
                                    ->get();

            $master_bill = $billingModel->masterBill($pnr_no, $request->price*count($request->passenger), $request->tax_amount*count($request->passenger), $request->agent_id); 


            foreach($request->passenger as $p){
                //new ticket, with pnr no, passnger ticket id, scheduler and lastly status 3[date changed]
                $newTicket = $billingModel->newTicket($pnr_no, $p, $request->scheduler_id, $request->agent_id, 3);
                
                //billing detail
                $billingModel->billDetail($master_bill->id, $request->price, $request->tax_amount, $newTicket->id, $p, $request->agent_id);
            }
            
            //$billingModel->refund($noshow, $masterBillId_asPerPnr, $request->penalty * count($request->penalty), $request->agent_id);
        });
    }
    
    /**
     * search page after passing parameters
     * @param  Request $request [description]
     * @param  [type]  $arrival [description]
     * @return [type]           [description]
     */
    public function search(Request $request,$arrival = null){
        $currency = AgentDetail::select('billing_currency')->where('user_id',Auth::user()->id)->first();
        $city = new City;
        $rbd = new Rbd;
        $class = new cabin;
        
        //search for one way trip
        $cabin = Cabin::all();
        $scheduler = Scheduler::where('source',$request->source)
                     ->where('destination',$request->destination)
                     ->where('date',$request->fromDate)
                     ->where('cabin_id',$request->class)
                     ->get();

        if($scheduler->count() < 1){
            return redirect('agent/dashboard')->withErrors(['*leaving flight not found']);        
        }
        // dd($scheduler);

        // search for round trip
        if($request->trip == 'round'){
            $arrival = Scheduler::where('destination',$request->source)
                         ->where('source',$request->destination)
                         ->where('date',$request->toDate)
                         ->where('cabin_id',$request->class)
                         ->get();

            if($arrival->count() < 1){
                return redirect('agent/dashboard')->withErrors(['*returning flight not found']);    
            }
        }
           // dd($scheduler);
        return view('frontend.search_page.index',compact('request','scheduler','city','rbd','arrival','currency','cabin','class'));
    }
    
    /**
     * save the passanger requst
     * @param  Request $request [description]
     * @return [type]           [description]
    */
    public function reserve(Request $req){

        DB::transaction(function() use($req, &$passenger_id, &$i){
            //dd($req->all());
            $tax = DB::table('taxes')
                ->select('id', 'local_price', 'tax_head','local_price', 'usd_price')->where('active',1)
                ->get();

            //dd($tax);    
            
            $pnr_no = str_random(8);   
            /**
             * multiple price to every passenger for total bill amoun
             * @var [type]
             */
            $master_bill = new MasterBill();
            /*-----if this is round-trip then save totals of both in master bill-----*/
            if($req->price_radio1 != null){
               $master_bill->amount= $req->total_fare_round+$req->total_fare;
               $master_bill->tax_amount= $req->total_tax+$req->total_tax_round;
            }
            /*if this is one-way then save in master bill */
            else{
                $master_bill->amount = $req->price_radio * count($req->form);//just $req->total_fare also works
                $master_bill->tax_amount = $req->total_tax;
            }

            $master_bill->agent_id=Auth::user()->id;


            $master_bill->pnr = $pnr_no;
            $master_bill->save();
            

            $currency = AgentDetail::where('user_id', Auth::user()->id)
                                    ->value('billing_currency');

            foreach ($req->form as $key => $value) {
                $passenger = new Passenger();
                $passenger->fname = $value['fname'];
                $passenger->mname = $value['mname'];
                $passenger->lname = $value['lname'];
                $passenger->nationality_id = $value['nationality_id'];
                $passenger->dob = $value['dob'];
                $passenger->expiry_date = $value['expiry_date'];
                $passenger->issued_date = $value['issued_date'];
                $passenger->meal_id = $value['meal_id'];
                $passenger->seat_id = $value['seat_id'];
                $passenger->mobile_no = $value['mobile_no'];
                $passenger->email = $value['email'];
                $passenger->passport = $value['passport'];
                $passenger->save();
                
                $passenger_id = $passenger->id;
                //$tiket_id = 0;
                /**
                 * find the latest ticket no and add by 1 so that your can
                 * increment ticket id
                 * @var [type]
                 */
                $max_id = DB::table('tickets')->max('id');
                $ticket = new Ticket();
                $ticket->pnr = $pnr_no;
                $ticket->ticket_no = 'ticket@prefix'.$max_id;   
                $ticket->status = 0;
                $ticket->passenger_id = $passenger->id;
                $ticket->agent_id = Auth::user()->id;
                $ticket->save();
                /**
                 * maintain the ticket and scheduler seprate table
                 */
                foreach ($req->scheduler_id as $key => $value) {
                    DB::table('ticket_scheduler')->insert([
                        'ticket_id' => $ticket->id,
                        'scheduler_id' => $value
                    ]);
                }
                
                /**
                 * bill detail for each bill
                 */
                $bill_detail = new BillDetail();
                $bill_detail->master_bill_id = $master_bill->id; 
                $bill_detail->agent_id=Auth::user()->id;

                /*if round trip then save each passenger's bill will be split from the total bill*/
                if($req->price_radio1 != null){
                   $bill_detail->amount= ($req->total_fare_round+$req->total_fare)/count($req->form);
                   $bill_detail->tax_amount= ($req->total_tax+$req->total_tax_round)/count($req->form);
                }
                /*----if this is one way trip------*/
                else{
                    $bill_detail->amount = $req->price_radio;
                    $bill_detail->tax_amount = ($req->total_tax)/count($req->form);
                }

                $bill_detail->refund = 0;
                $bill_detail->status = 0;
                $bill_detail->ticket_id = $ticket->id;
                $bill_detail->passenger_id = $passenger->id;
                $bill_detail->remarks = "hello";
                $bill_detail->save();
                
                //add tax for every bill
                //if usd
                if($currency=="USD"){
                    foreach ($tax as $key => $value) {

                    //---if this is round trip then:
                        if($req->price_radio1 != null){

                        $tax_detail_pivot = DB::table('tax_price_pivot')
                                    ->insert([
                                        'tax_id' => $value->id,
                                        'tax_head' => $value->tax_head,
                                        'tax_percentage' => ($value->usd_price)*100,
                                        'amount' => $value->usd_price*$req->total_fare_round,
                                        'bill_detail_id' => $bill_detail->id,
                                        'passenger_id' => $passenger->id
                                    ]);
                        }
                        /*---if this is single trip---*/
                            $tax_detail_pivot = DB::table('tax_price_pivot')
                                    ->insert([
                                        'tax_id' => $value->id,
                                        'tax_head' => $value->tax_head,
                                        'tax_percentage' => ($value->usd_price)*100,
                                        'amount' => $value->usd_price*$req->total_fare,
                                        'bill_detail_id' => $bill_detail->id,
                                        'passenger_id' => $passenger->id
                                    ]);
                    }
                }
                //if npr
                else{
                    foreach ($tax as $key => $value) {
                        //if round trip
                        if($req->price_radio1 != null){

                            $tax_detail_pivot = DB::table('tax_price_pivot')->insert([
                            'tax_id' => $value->id,
                            'tax_head' => $value->tax_head,
                            'tax_percentage' => ($value->local_price)*100,
                            'amount' => $value->local_price*$req->total_fare_round,
                            'bill_detail_id' => $bill_detail->id,
                            'passenger_id' => $passenger->id
                        ]);

                        }
                        //for single trip

                            $tax_detail_pivot = DB::table('tax_price_pivot')->insert([
                            'tax_id' => $value->id,
                            'tax_head' => $value->tax_head,
                            'tax_percentage' => ($value->local_price)*100,
                            'amount' => $value->local_price*$req->total_fare,
                            'bill_detail_id' => $bill_detail->id,
                            'passenger_id' => $passenger->id
                        ]);
                    }
                }
            }
        });

        if(isset($req->goBack)){
            return redirect('agent/dashboard');            
        }

        return redirect('reservation/ticket/print/'.$passenger_id);
    }
    /**
     * refund no show
     */
    public function refund(Request $req, MasterBill $masterBill){
        DB::transaction(function() use($req, &$masterBill){
            
            /**
             * get master bill id as per pnr no
             * one pnr no equal to one master bill
             */
            $masterBillId_asPerPnr = $masterBill->id_as_pnr($req->pnr);
            /**
             * 1. fetch amount of particular bill and store in $noshow
             * 2. total amount from $noshow and get as total sum [$total_amt]
             * 2. fetch tax same as well [$total_tax]
             */
            
            $total_amt = 0;
            $total_tax = 0;
            $noshow = DB::table('bill_details')->select('amount', 'tax_amount', 'passenger_id', 'ticket_id','agent_id')
                                    ->where('master_bill_id', $masterBillId_asPerPnr)
                                    ->get();
                                     
            foreach($noshow as $ns){
                $total_amt = $total_amt + $ns->amount; 
                $total_tax = $total_tax + $ns->tax_amount; 
            }
            
            /**
             * make master refund refrence from master bill id
             * and total amount [$total_amt] and tax amount [$total_tax]
             * @var MasterRefund
             */
            $master_refund = new MasterRefund();
            $master_refund->master_bill_id = $masterBillId_asPerPnr;
            $master_refund->amount = $total_amt;
            $master_refund->tax_amount = $total_tax;
            $master_refund->save();
            
            
            /**
             * create master bill for refunded bill
             * which will include penalty excluding tax
             * $pnr no will be as generated randomly [str_random(8)]
             * @var [type]
             */
            $pnr_no = str_random(8);   
            $master_bill = new MasterBill();
            $master_bill->amount =$req->penalty * count($req->penalty); 
            $master_bill->tax_amount = 0;
            $master_bill->pnr = $pnr_no;
            $master_bill->save();
            

            /**
             * 1. $nosow contains every bill detail
             * 2. refund detail will be created as per $noshow data
             */
            foreach($noshow as $ns){
                $refund_detail = new RefundDetail();
                $refund_detail->master_refund_id = $master_refund->id;
                $refund_detail->amount = $ns->amount;
                $refund_detail->tax_amount = $ns->tax_amount;
                $refund_detail->passenger_id = $ns->passenger_id;
                $refund_detail->save();
                
                /**
                 * since no show and cancle behave same expect thier status and penalty may differ
                 * we redirect both request to same function
                 * we set cancle param to distinguesh either request is form canle or no show
                 **/

                if(isset($req->cancle)){
                    $status = 3;    
                }else{
                    $status = 2;
                }

                /**
                 * bill detail will be created as per refund detail
                 * except penalty will be added
                 * @var BillDetail
                 */
                $bill_detail = new BillDetail();
                $bill_detail->master_bill_id = $master_bill->id; 
                $bill_detail->amount = $req->penalty;
                $bill_detail->tax_amount = 0;
                $bill_detail->refund = $refund_detail->id;
                $bill_detail->status = $status;
                $bill_detail->ticket_id = $ns->ticket_id;
                $bill_detail->passenger_id = $refund_detail->passenger_id;
                $bill_detail->remarks = "hello";
                $bill_detail->save();
                
                /**
                 * update status to refunded which will indicated this bill was refunded
                 */
                DB::table('tickets')->where('id', $bill_detail->ticket_id)
                                    ->update(['status' => $status]);
            }
        });
        
        return back()->withSuccess(['Successfully cancle']);
    }
    
    /**
     * reservae complition
     * @return [type] [description]
     */
    public function reserve_complete()
    {
        return view('frontend.search_page.ticket_print');
    }
    /**
     * redirect after
     * reserving a
     * flight
     * 
     * @return [type] [description]
     */
    public function ticket_print($passenger_id, Ticket $ticket)
    {
        /**
         * increment number for serial number in dispaly table
         * @var integer
         */
        $i = 1;
        /**
         * 1. fetch pnr number as the passenger id form ticket :
         * since pnr group the ceratain tickets, firt we fetch pnr number for
         * $passenger_id which is para for this method
         * 2. we fetch all tickets under this pnr no and store in $passenger_ticket
         * @var [type]
         */
        $tickets = DB::table('tickets')->where('passenger_id', $passenger_id)->select('id', 'pnr')->first();
        $passenger_ticket = $ticket->ticket()
                          ->where('pnr', $tickets->pnr)
                          ->all();
        /**
         * $ticket->scheduler() is method defined in Ticket model which
         * fetch the all scheduler related to this ticket, this related scheduler have realation
         * with flight so that we can fetch source, destination etc
         */
        
        $scheduler_ticket = $ticket->scheduler_ticket_pivot($tickets->id);
        
        /**
         * in ticket print "FLIGHT INFORMATION" section need master ticket detail
         * we can get master detail information from $ticket[0]
         * else we can loop through $ticket
         */
        if(count($ticket) > 1){
            $master_ticket = $ticket[0];
        }else{
            $master_ticket = $ticket;
        }
        return view('frontend.print',compact('passenger_ticket','master_ticket', 'i', 'scheduler_ticket'));
    } 
    /**
     * display no show form
     * with all passengers fo
     * particular pnr no
     */
    public function noShow($pnr_no, Ticket $ticket){
        $i = 1;

        /**
         * $ticket->as_pnr($pnr_no) function is defined in Ticket model
         * it get all tickets with status 0 where pnr no is $pnr_no
         * @var [type]
         */

        $ticket = $ticket->as_pnr($pnr_no);
        /**
         * if no ticket with status 0
         * send no active data result
         */
        if($ticket->count() < 1){
            return back()->withErrors(['no active data']);
        }
        return view('backend.reservation.noshow', compact('ticket', 'i', 'pnr_no'));
    }
    /**
     * cancleation
     * the reservation
     */
    public function cancel($pnr_no, Ticket $ticket){
        $i = 1;

        $ticket = $ticket->as_pnr($pnr_no);
        /**
         * if no normal
         * ticket 
         */
        if($ticket->count() < 1){
            return back()->withErrors(['no active data']);
        }
        return view('backend.reservation.cancellation', compact('ticket', 'i', 'pnr_no'));

    }
}
