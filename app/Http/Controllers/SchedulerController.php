<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Flight;
use App\Cabin;
use App\Rbd;
use App\Fare;
use App\Schedule;
use App\Scheduler;
use App\City;
use Session;
use DB;
use Carbon\Carbon;
use App\Http\Requests\SchedulerEditRequest;
use App\Http\Controllers\Controller;

class SchedulerController extends Controller
{   
    public function index()
    {
        $scheduler = Scheduler::select('lot_no','flight_id','date_from','date_to','depart_time','arrive_time')->distinct()->get();
        // dd($scheduler);
        $city = new City;
        $schedule = new Schedule;
        return view('backend.scheduler.index',compact('scheduler','city','schedule'));
    }

    public function show($id)
    {
        $scheduler = Scheduler::select()->where('lot_no',$id)->orderBy('date','asc')->get();
        $city = new City;
        $rbd = new Rbd;
        $schedule = new Schedule;
        return view('backend.scheduler.show',compact('scheduler','city','schedule','rbd'));
    }


    public function create(){
      $schedules = Schedule::select('flight_id')->get();
      $city = new city;

      return view('backend.scheduler.create',compact('city'))->with('schedules',$schedules);
    }

    public function loader(Request $request)
    {   
        $this->validate($request, [
            'flight_id' => 'required',
            'date_from' => 'required|date',
            'date_to' => 'required|date|after:date_from',
        ]);
        $scheduler = new Scheduler;
        $lot = $scheduler->getMax();
        $city = new City;
        $flight_id = $request->flight_id;
        $date_from = $request->date_from;
        $date_to = $request->date_to;
        // dd($scheduler->dateDayRange($date_from,$date_to));
        $dates=$scheduler->dateDayRange($date_from,$date_to);
        $schedule = Schedule::select()
                        ->where('flight_id',$flight_id)
                        ->first();
        // Schedule::select()->where('flight_id',$flight_id)->get();
        $valid_days = DB::table('schedule_week_pivot')->select('week_id')->where('schedule_id',$schedule->id)->get();
        $flight_dates = [];
        foreach ($valid_days as $key => $value) {
            $day = (string)$value->week_id;
            $flight_dates = array_merge($flight_dates, $dates[$day]);
        }

        $fare = DB::table('fares')
              ->leftJoin('cabins','fares.cabin_id','=','cabins.id')
              ->leftJoin('rbds','fares.rbd_id','=','rbds.id')
              ->select('fares.id','fares.price_local_oneway','fares.price_usd_oneway','fares.price_local_roundtrip','fares.price_usd_roundtrip','fares.seat','fares.cabin_id','cabins.cabin','fares.rbd_id','rbds.rbd')
              ->orderBy('cabins.id','asc')
              ->orderBy('rbds.hierarchy','asc')
              ->get();
              // dd($fare);

        // dd($dates[1]);
        // dd($valid_days[0]->week_id);
        return view('backend.scheduler.scheduler_index',compact('flight_dates','schedule','scheduler','fare','city','date_from','date_to','lot'));

    }
    public function store(Request $request)
    {

      DB::transaction(function() use ($request){
         foreach ($request->scheduler as $key => $value) {
          $scheduler = new Scheduler();
          $scheduler->date_from = Carbon::parse($request->date_from);
          $scheduler->date_to = Carbon::parse($request->date_to);
          $scheduler->date = Carbon::parse($value['date']);
          $scheduler->date_depart = $value['d_date'];
          $scheduler->flight_id = $request->flight_id;
          $scheduler->rbd_id = $value['rbd_id'];
          $scheduler->cabin_id = $value['cabin_id'];
          $scheduler->seat =  $value['seat'];
          $scheduler->price_local_oneway = $value['price_local_oneway'];
          $scheduler->price_usd_oneway =  $value['price_usd_oneway'];
          $scheduler->price_local_roundtrip = $value['price_local_roundtrip'];
          $scheduler->price_usd_roundtrip = $value['price_usd_roundtrip'];
          $scheduler->depart_time = $request->depart_time;
          $scheduler->arrive_time = $request->arrive_time;
          $scheduler->lot_no = $request->lot_no;
          $scheduler->source = $request->source;
          $scheduler->destination = $request->destination;
          $scheduler->save();
        }
    });

    Session::flash('success','Schedule has been Generated');

    return redirect(url('/scheduler'));
      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $schedules = Schedule::select('flight_id')->get();
      $city = new city;
      $rbd = new Rbd;
      $cabin =  new Cabin;
      $scheduler = Scheduler::find($id);

      return view('backend.scheduler.edit',compact('city','scheduler','rbd','cabin'))->with('schedules',$schedules);
    }

    public function update(SchedulerEditRequest $request,$id)
    {

      $scheduler = Scheduler::find($id);
       DB::transaction(function () use ($request, $scheduler)
        {
           
            $scheduler->update($request->fill());

          
        });

        Session::flash('success','Selected Scheduler has been Updated');

        return redirect()->back();
    }

    public function destroy($id)
    {
        //

        $scheduler = Scheduler::find($id);
         if ($scheduler->delete())
        {
            Session::flash('success','Selected Scheduler has been deleted');

            return redirect()->back();
        }

    }

    public function delete($id)
    {
        //

        
            Scheduler::where('lot_no', '=', $id)->delete();
            Session::flash('success','Selected Scheduler has been deleted');

            return redirect()->back();


    }
}