<?php

namespace App\Http\Controllers;

use App\Cabin;
use App\City;
use App\Fare;
use App\Flight;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Rbd;
use Illuminate\Http\Request;
use App\Http\Requests\FareRequest;
use Illuminate\Support\Facades\Validator;
use DB;

class FareController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $flight = Flight::all();
      $city = new City;

      return view('backend.fare.index',compact('city'))->withFlights($flight);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $flight = Flight::all();
        $city = new City;
        $rbd = Rbd::all();
        $cabin = new Cabin;
        return view('backend.fare.create',compact('city'))->withFlights($flight)
                                                          ->withRbds($rbd)
                                                          ->withCabin($cabin);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      DB::transaction(function () use ($request){
        foreach ($request->farePrice as $key => $value) {

          if ($value['seat'] != '0'){
            $fare = new Fare();
            $fare->flight_id = $request->flight_id;
            $fare->cabin_id = $value['cabin'];
            $fare->rbd_id = $value['rbd'];
            $fare->price_local_oneway = $value['onewayNpr'];
            $fare->price_usd_oneway =  $value['onewayUsd'];
            $fare->price_local_roundtrip = $value['roundTripNpr'];
            $fare->price_usd_roundtrip = $value['roundTripUsd'];
            $fare->seat =  $value['seat'];

            $fare->save();
          }

        }
      });

        return redirect()->route('fare.index')->with(['success'=>'Succesfuly, Created Fare !']);
    }

    public function check(Request $req){

      $flight_id = $req->flight_id;
      $flightDesc = Flight::select('flight_no','source_id','destination_id')->where('id','=',$flight_id)->first();
      // dd($flightDesc->source_id);

      if (Fare::where('flight_id','=', $flight_id)->exists()){

        $city = new City;
        $rbd = new Rbd;
        $rbds = Rbd::orderBy('hierarchy', 'asc')->get();
        $cabin = new Cabin;
        $faredetails = new Fare;

        return view('backend.fare.edit', compact('req'))->withFaredetails($faredetails)
                                        ->withCabin($cabin)
                                        ->withRbd($rbd)
                                        ->withFlight_id($flight_id)
                                        ->withFlightdesc($flightDesc)
                                        ->withCity($city)
                                        ->withRbdall($rbds);
      }
      else {
        $city = new City;
        $rbd = Rbd::orderBy('hierarchy', 'asc')->get();
        $cabin = new Cabin;

        return view('backend.fare.create')->withRbds($rbd)
                                          ->withCity($city)
                                          ->withCabin($cabin)
                                          ->withFlight_id($flight_id)
                                          ->withFlightdesc($flightDesc);

      }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      DB::transaction(function () use ($request,$id){

            $fares = Fare::where('flight_id','=',$id)->get();
            foreach ($request->farePrice as $fareprice) {

                  if (isset($fareprice['fare_id'])) {
                    foreach ($fares as $key => $fare) {
                      if ($fareprice['fare_id'] == $fare->id) {
                        $fare->price_local_oneway = $fareprice['onewayNpr'];
                        $fare->price_usd_oneway = $fareprice['onewayUsd'];
                        $fare->price_local_roundtrip = $fareprice['roundTripNpr'];
                        $fare->price_usd_roundtrip = $fareprice['roundTripUsd'];
                        $fare->seat = $fareprice['seat'];

                        $fare->save();
                      }
                    }
                  }
                  else {
                    if ($fareprice['seat'] != '0'){
                        $newfare = new Fare();
                        $newfare->flight_id = $request->flight_id;
                        $newfare->cabin_id = $fareprice['cabin'];
                        $newfare->rbd_id = $fareprice['rbd'];
                        $newfare->price_local_oneway = $fareprice['onewayNpr'];
                        $newfare->price_usd_oneway =  $fareprice['onewayUsd'];
                        $newfare->price_local_roundtrip = $fareprice['roundTripNpr'];
                        $newfare->price_usd_roundtrip = $fareprice['roundTripUsd'];
                        $newfare->seat =  $fareprice['seat'];
                        $newfare->save();
                    }
                  }
            }
      });
      return redirect()->route('fare.index')->with(['success'=>'Succesfuly, Update Fare !']);
    }






    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
