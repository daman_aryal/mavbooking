<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TransactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function main_passenger(){
        $inputs = [
            'fname' => $this->fname,
            'mname' => $this->mname,
            'lname' => $this->lname,
            'nationality_id' => $this->nationality,
            'passport' => $this->passport,
            'issued_date' => $this->issued_date,
            'expired_date' => $this->expired_date,
            'meal_id' => $this->meal_id,
            'seat_id' => $this->seat_id,
            'mobile_no' => $this->mobile_no,
            'email' => $this->email
        ];

        return $inputs;    
    }

    public function transaction()
    {
        $inputs = [
            'leaving_scheduler_id' => $this->leaving_scheduler_id,
            'returning_scheduler_id' => $this->returning_scheduler_id
        ];
        return $inputs;
    }

}
