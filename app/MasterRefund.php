<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterRefund extends Model
{
    protected $table = 'master_refunds';

   	protected $fillable = [
   		'master_bill_id', 'amount', 'tax_amount'
   	];
}
