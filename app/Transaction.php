<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transactions';

    protected $fillable = ['leaving_scheduler_id', 'returning_scheduler_id'
    	'main_passenger_id', 'created_by', 'status'
    ]
}
