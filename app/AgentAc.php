<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentAc extends Model
{
    protected $table = "agent_ac";

    protected $fillable = [
    	'user_id', 'billing_currency', 'balance'
    ];

    /**
     * get agent
     * depositing currentcy
     * either usd or local
     */
    public function currency($user_id)
    {
    	return static::where('id', $user_id)
    				->value('billing_currency');
    }

    
}
