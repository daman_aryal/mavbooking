<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Ticket extends Model
{
    protected $table = 'tickets';
    protected $fillable = [
    	'pnr', 'status', 'scheduler_id', 'passenger_id', 'agent_id'
    ];

    /**
     * 
     */
    public function passenger(){
    	return $this->belongsTo('App\Passenger','passenger_id', 'id');
    }

    /**
     * fetch ticket detail
     * with eagor loading
     * @return [type] [description]
     */

    public function ticket(){
        $ticket =Ticket::with('passenger')->paginate(10);
        return $ticket;
    }

    public function getAgent($agent_id)
    {
        $agent_name = User::select('name')->where('id',$agent_id)->first();
        return $agent_name->name;
    }

    public function getFlightno($flight_id)
    {
        $flight_no=DB::table('flights')->select('flight_no')->where('id',$flight_id)->first();
        return $flight_no->flight_no;
    }

    public function as_pnr($pnr){
        $ticket = Ticket::with('passenger')
                ->where('pnr', $pnr)
                ->where('status', 0)
                ->get(['id', 'passenger_id', 'status', 'pnr', 'agent_id' ,'ticket_no']);
        return $ticket;    
    }
    /**
     * ticket has many schedulers
     */
    public function scheduler_ticket_pivot($ticket_id){
        $scheduler_in_ticket = [];
        $scheduler_id_in_pivot = DB::table('ticket_scheduler')->where('ticket_id', $ticket_id)->select('scheduler_id')->get();
        foreach ($scheduler_id_in_pivot as $key => $value) {
            $scheduler_in_ticket[] = Scheduler::with('flight','flight.source','flight.destination', 'cabin', 'cabin.rbd')->where('id', $value->scheduler_id)->get();
        }
        return array_collapse($scheduler_in_ticket);
    }

   
}
