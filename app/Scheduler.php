<?php

namespace App;

use App\AgentDetail;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Scheduler extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date_from','date_to', 'flight_id','rbd_id','seat','depart_time','arrive_time','date_depart', 'date','lot_no'
    ];

     public function flight()
    {
        return $this->belongsTo('App\Flight','flight_id','id');
    }
	public function getDate($date)
	{
		return Carbon::parse($date)->format('Y-m-d');
	}

	public function cabin()
	{
		return $this->hasOne('App\Cabin', 'id', 'cabin_id');
	}

	public function dateDayRange($start_date,$last_date)
	{

		$dates = range(strtotime($start_date), strtotime($last_date),86400);
		$days = array();

		array_map(function($v)use(&$days){
		        if(date('D',$v) == 'Sun'){
		            $days['1'][] = date('Y-m-d', $v);
		        }elseif(date('D',$v) == 'Mon'){
		            $days['2'][] = date('Y-m-d', $v);
		        }elseif(date('D', $v) == 'Tue'){
		            $days['3'][] = date('Y-m-d', $v);
		        }elseif(date('D',$v) == 'Wed'){
		            $days['4'][] = date('Y-m-d', $v);
		        }elseif(date('D', $v) == 'Thu'){
		            $days['5'][] = date('Y-m-d', $v);
		        }elseif(date('D',$v) == 'Fri'){
		            $days['6'][] = date('Y-m-d', $v);
		        }elseif(date('D',$v) == 'Sat'){
		            $days['7'][] = date('Y-m-d', $v);
		        }
		    }, $dates);

		return $days;
	}

	public function getMax()
	{
		$scheduler = Scheduler::max('id');
		if (!$scheduler)
		{
			return 1;
		}
		return $scheduler;
	}

	protected function price_mode($agent_id, $scheduler_id)
	{
		$billing_currency = AgentDetail::where('id', $agent_id)->value('billing_currency');
		$scheduler_price = DB::table('schedulers')
							->where('id', $scheduler_id)
							->select('price_local_oneway', 'price_usd_oneway', 'price_local_roundtrip', 'price_usd_roundtrip')
							->first();
		$price = [
			'billing_currency' => $billing_currency,
			'scheduler_price' => $scheduler_price
		];

		return $price;
	}
	
	public function oneway_price_mode($agent_id, $scheduler_id)
	{
		if($this->price_mode($agent_id, $scheduler_id)['billing_currency'] == "USD"){
			$oneway_price = "$ " . $this->price_mode($agent_id, $scheduler_id)['scheduler_price']->price_usd_oneway;
		}else{
			$oneway_price = "Rs " . $this->price_mode($agent_id, $scheduler_id)['scheduler_price']->price_local_oneway;
		}
		return $oneway_price;
	}

	public function roundtrip_price_mode($agent_id, $scheduler_id)
	{
		if($this->price_mode($agent_id, $scheduler_id)['billing_currency'] == "USD"){
			$roundtrip = "$ " . $this->price_mode($agent_id, $scheduler_id)['scheduler_price']->price_usd_roundtrip/2;
		}else{
			$roundtrip = "Rs " . $this->price_mode($agent_id, $scheduler_id)['scheduler_price']->price_usd_roundtrip/2;
		}
		return $roundtrip;
	}
}
	