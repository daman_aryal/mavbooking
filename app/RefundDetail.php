<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefundDetail extends Model
{
    protected $table = 'refund_details';
    protected $fillable = [
    	'master_refund_id', 'amount', 'tax_amount',
    	'passenger_id'
    ];
}
