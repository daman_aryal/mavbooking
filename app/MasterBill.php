<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterBill extends Model
{
    protected $table = 'master_bills';

    protected $fillable = [
    	'amount', 'tax_amount', 'pnr'
    ];

    public function id_as_pnr($pnr){
    	$id = MasterBill::where('pnr', $pnr)
                            ->value('id');
       return $id;
    }

    // public function getAgentN($){
    	
    // }
}
