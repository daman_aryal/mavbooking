<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Passenger extends Model
{
    protected $table = 'passengers';
   	protected $fillable = [
    	'fname', 'lname','mname','nationality_id', 'passport',
    	'issued_date', 'expiry_date', 'meal_id', 'seat_id',
    	'mobile_no', 'email', 'dob'	
    ];
	
	protected $dates = ['issued_date', 'expiry_date', 'dob'];
}
