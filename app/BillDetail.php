<?php

namespace App;

use App\BillDetail;
use Illuminate\Database\Eloquent\Model;

class BillDetail extends Model
{
    protected $table = "bill_details";
    protected $fillable = [
    	'master_bill_id'. 'amount', 'tax_amount',
    	'refund', 'status', 'passenger_id',
    	'ticket_id', 'remarks'
    ];

    /**
     *  bill has one
     *  ticket
     */
    public function ticket(){
        return $this->belongsTo('App\Ticket', 'ticket_id', 'id');
    }

    /**
     * belongs to many relationship
     * to tax table
     */
    public function tax(){
    	return $this->belongsToMany('App\Tax', 'tax_price_pivot', 'tax_id', 'bill_detail_id')->withPivot([
    		'tax_head', 'tax_percentage', 
    		'amount', 'passenger_id'
    	])->withTimestamps();
    }

    /**
     * bill with
     * egar 
     * loading
     */
    
    public function bill(){
        $ticket =BillDetail::with('ticket.passenger','ticket.scheduler.flight','ticket.scheduler.flight.source', 'ticket.scheduler.flight.destination')->get();
        return $ticket;
    }

    public function getPassengerN($passenger_id){
        $pname=Passenger::select('fname','mname','lname')->where('id',$passenger_id)->first();
        return $pname->fname." ".$pname->mname." ".$pname->lname;
    }
}
    