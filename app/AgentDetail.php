<?php

namespace App;

use App\AgentDetail;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Ticket;

class AgentDetail extends Model
{
    protected $table = "agent_details";


    public function billing_currency($agent_id)
    {
    	$billing_currency = AgentDetail::where('id', $agent_id)->value('billing_currency');
    	return $billing_currency;
    }

    public function getAgentName($user_id)
    {
    	$agent_name = User::select('name')->where('id',$user_id)->first();
    	return $agent_name->name;
    }
    
    public function getCurrency($agent_id){
        $currency=AgentDetail::select('billing_currency')->where('user_id',$agent_id)->first();
        return $currency->billing_currency;
    }
}
