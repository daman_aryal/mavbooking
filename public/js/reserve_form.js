var FormWizard = function () {
    return {
        //main function to initiate the module
        init: function () {
            if (!jQuery().bootstrapWizard) {
                return;
            }
            


            var form = $('#submit_form');
            var error = $('.alert-danger', form);
            var success = $('.alert-success', form);
            
            
            
                form.validate({
                    doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
                    errorElement: 'span', //default input error message container
                    errorClass: 'help-block help-block-error', // default input error message class
                    focusInvalid: false, // do not focus the last invalid input
                    rules: {
                        //account
                        price_radio: {
                            required: true,
                        },
                        //profile
                        price_radio1: {
                            required: true
                        }
                    },

                    messages: { // custom messages for radio buttons and checkboxes
                        'price_radio' : {
                            required:  `<div class="alert alert-danger" role="alert">
                                          * Please select departure flight.
                                        </div>`
                        },
                        'price_radio1' : {
                            required: `<div class="alert alert-danger" role="alert">
                                          Please select return flight.
                                        </div>`  
                        }

                    },

                    errorPlacement: function (error, element) { // render error placement for each input type
                        if (element.attr("name") == "price_radio") { // for uniform radio buttons, insert the after the given container
                            error.insertAfter("#departure");
                        } else if (element.attr("name") == "price_radio1") { // for uniform checkboxes, insert the after the given container
                            error.insertAfter("#return");
                        } else {
                            error.insertAfter(element); // for other inputs, just perform default behavior
                        }
                    },

                    invalidHandler: function (event, validator) { //display error alert on form submit   
                        success.hide();
                        error.show();
                        App.scrollTo(error, -200);
                    },

                    highlight: function (element) { // hightlight error inputs
                        $(element)
                            .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
                    },

                    unhighlight: function (element) { // revert the change done by hightlight
                        $(element)
                            .closest('.form-group').removeClass('has-error'); // set error class to the control group
                    },

                    success: function (label) {
                        if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                            label
                                .closest('.form-group').removeClass('has-error').addClass('has-success');
                            label.remove(); // remove error label here
                        } else { // display success icon for other inputs
                            label
                                .addClass('valid') // mark the current input as valid and display OK icon
                            .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                        }
                    },
                    submitHandler: function (form) {
                        success.show();
                        error.hide();

                        var go_back = 1;

                        $("#save_and_print").click(function () {
                            form.submit();
                        });
                        $("#save_and_back").click(function () {
                            var input = $("<input>")
                                        .attr("type", "hidden")
                                        .attr("name", "goBack").val("goBack");
                            $("#submit_form").append(input);
                            form.submit();
                        })
                        //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
                    },

                }); 
                
                /*----new method for mobile number validation---*/    
                $.validator.addMethod(
                    "regex",function(value, element, regexp) {
                        var re = new RegExp(regexp);
                        //console.log(re);
                        return this.optional(element) || re.test(value);
                    },"Invalid Mobile Number"
                );                
                
                /*----each field validation----*/
                $("#passenger_detail").click(function(){
                    var fields = [
                        '.fname','.lname', 
                        '.nationality', '.dob','.issued_date', '.exp_date', 
                        '.meal','.seat', '.email', '.passport'
                    ];

                    $.each(fields, function(key, val){
                        $(val).each(function(){
                            $(this).rules("add", {
                                required : true,
                                messages: {
                                    required: "<span style='color:red'>* required.</span>"
                                }
                            });               
                        });    
                    });
                    /*--mobile no. validation--*/
                    var field2=['.mobile'];
                    $.each(field2,function(key,val){
                        $(val).each(function(){
                            $(this).rules("add", {
                                required : true,
                                minlength: 9,
                                maxlength:15,
                                regex:"^[0-9\+]{1,}[0-9\-]{3,15}$",
                                messages: {
                                    //regex:"<span style='color:red'>Validation failed</span>",
                                    required: "<span style='color:red'>* required.</span>",
                                    minlength:"<span style='color:red'>* Minlength of 9 required</span>",
                                    maxlength:"<span style='color:red'>* Maxlength of 15 required</span>"
                                }
                            });               
                        });   
                    });

                    $(".email").rules("add", function(){
                        required : true;
                        messages : {
                            required : "<span style='color:red'>* required.</span>"
                        }
                    }); 
                     
                });
                
           
            
            /**
             * form wizard
             * components 
             */
            var displayConfirm = function() {
                $('#tab3 .form-control-static', form).each(function(){
                    var input = $('[name="'+$(this).attr("data-display")+'"]', form);
                    if (input.is(":radio")) {
                        input = $('[name="'+$(this).attr("data-display")+'"]:checked', form);
                    }
                    if (input.is(":text") || input.is("textarea")) {
                        $(this).html(input.val());
                    } else if (input.is("select")) {
                        $(this).html(input.find('option:selected').text());
                    } else if (input.is(":radio") && input.is(":checked")) {
                        $(this).html(input.attr("data-title"));
                    } else if ($(this).attr("data-display") == 'payment[]') {
                        var payment = [];
                        $('[name="payment[]"]:checked', form).each(function(){ 
                            payment.push($(this).attr('data-title'));
                        });
                        $(this).html(payment.join("<br>"));
                    }
                });
            }

            var handleTitle = function(tab, navigation, index) {
                var total = navigation.find('li').length;
                var current = index + 1;
                // set wizard title
                $('.step-title', $('#form_wizard_1')).text('Step ' + (index + 1) + ' of ' + total);
                // set done steps
                jQuery('li', $('#form_wizard_1')).removeClass("done");
                var li_list = navigation.find('li');
                for (var i = 0; i < index; i++) {
                    jQuery(li_list[i]).addClass("done");
                }

                if (current == 1) {
                    $('#form_wizard_1').find('.button-previous').hide();
                } else {
                    $('#form_wizard_1').find('.button-previous').show();
                }

                if (current >= total) {
                    $('#form_wizard_1').find('.button-next').hide();
                    $('#form_wizard_1').find('.button-submit').show();
                    displayConfirm();
                } else {
                    $('#form_wizard_1').find('.button-next').show();
                    $('#form_wizard_1').find('.button-submit').hide();
                }
                App.scrollTo($('.page-title'));
            }

            // default form wizard
              $('#form_wizard_1').bootstrapWizard({
                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',
                onTabClick: function (tab, navigation, index, clickedIndex) {
                    return false;
                    
                    success.hide();
                    error.hide();
                    if (form.valid() == false) {
                        return false;
                    }
                    
                    handleTitle(tab, navigation, clickedIndex);
                },
                onNext: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    if (form.valid() == false) {
                        return false;
                    }

                    handleTitle(tab, navigation, index);
                },
                onPrevious: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    handleTitle(tab, navigation, index);
                },
                onTabShow: function (tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    var $percent = (current / total) * 100;
                    $('#form_wizard_1').find('.progress-bar').css({
                        width: $percent + '%'
                    });
                }
            });

            $('#form_wizard_1').find('.button-previous').hide();
            $('#form_wizard_1 .button-submit').click(function () {
                alert('Finished! Hope you like it :)');
            }).hide();

            

            //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
            $('#country_list', form).change(function () {
                form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });
        }

    };

}();

jQuery(document).ready(function() {
    FormWizard.init();
});