<?php

use App\Nationality;
use Illuminate\Database\Seeder;

class NationalitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $nationality = [
          ['name' => 'Nepali'],
          ['name' => 'Indian'],
          ['name' => 'Chinese'],
          ['name' => 'American'],
          ['name' => 'Britist'],
          ['name' => 'German']
        ];

        //creaete a super role    
        DB::table('nationality')->delete();
        foreach ($nationality as $key => $value) {
           Nationality::create($value);
        }
    }
}
