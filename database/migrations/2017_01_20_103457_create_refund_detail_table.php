<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefundDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('refund_details', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('master_refund_id')->unsigned()->nullable();
            $table->foreign('master_refund_id')->references('id')->on('master_refunds')->onDelete('cascade')->onUpdate('cascade');

            /*$table->integer('bill_detail_id')->unsigned()->nullable();
            $table->foreign('bill_detail_id')->references('id')->on('bill_details')->onDelete('cascade')->onUpdate('cascade');*/

            $table->float('amount');
            $table->float('tax_amount');

            $table->integer('passenger_id')->unsigned()->nullable();
            $table->foreign('passenger_id')->references('id')->on('passengers')->onDelete('cascade')->onUpdate('cascade');   
            $table->integer('agent_id')->unsigned()->nullable();
            $table->foreign('agent_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade'); 

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('refund_details');
    }
}
