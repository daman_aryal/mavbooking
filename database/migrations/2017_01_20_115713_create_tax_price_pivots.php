<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxPricePivots extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('tax_price_pivot', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('tax_id')->unsigned()->nullable();
            $table->foreign('tax_id')->references('id')->on('taxes')->onDelete('cascade')->onUpdate('cascade'); 

            $table->string('tax_head');
            $table->float('tax_percentage', 10, 2);   

            $table->float('amount', 8, 2);

            $table->integer('bill_detail_id')->unsigned()->nullable();
            $table->foreign('bill_detail_id')->references('id')->on('bill_details')->onDelete('cascade')->onUpdate('cascade');
            
            $table->integer('passenger_id')->unsigned()->nullable();
            $table->foreign('passenger_id')->references('id')->on('passengers')->onDelete('cascade')->onUpdate('cascade');    

            $table->softDeletes();
            $table->timestamps();
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tax_price_pivot');
    }
}
