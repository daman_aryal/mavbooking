@extends('backend.admin_master')

@section('contents')
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('/dashboard') }}">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Agent's Ticket</a>
            </li>
        </ul>
    </div>
    <h3 class="page-title"> All PNRs of {{ $agent_detail->getAgentName($agent_name->agent_id)}}
    </h3>
    
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>Agent's Tickets </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                    </div>
                </div>
                <div class="portlet-body flip-scroll">
                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="agent_tickets">
                        <thead class="flip-content">
                            <tr>
                                <th width="10%"> S.N.</th>
								<th>PNR #</th>
                                <th>Total Amount</th>
                                <th>Total Tax</th>
								<th>Options</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            
                            @foreach($agent_pnrs as $key => $agent_pnr)
                            <tr class="tr">
                                <td>{{ ++$key }}</td>
								<td>{{$agent_pnr->pnr}}</td>
                                <td>{{$currency->getCurrency($agent_pnr->agent_id)}}{{" "}}{{ $agent_pnr->amount}}</td>
                                <td>{{$currency->getCurrency($agent_pnr->agent_id)}}{{" "}}{{ $agent_pnr->tax_amount}}</td>
                                <td>

                                <div class="col-md-12" style="display:flex;">
                                    <button type="button" value="{{ $agent_pnr->id}}" class="show_ticket" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal">Show Tickets</button>
                                </div>
                                
                                </td>
                            </tr>
                            @endforeach
                        </tbody> 
                        <div class="modal fade" id="myModal">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                  <h4 class="modal-title">All Tickets Related to PNR</h4>
                                </div>
                                <div class="modal-body">
                                    <div id="result">
                            
                                      </div>
                                </div>

                                <div class="modal-footer">
                                  <button type="button" id="btn1" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                              </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                        
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer')
    <script>
        $("#agent_tickets").DataTable();

        $(".show_ticket").on("click",function(){
            var master_id=$(this).val();
            //console.log(master_id);
            $.ajax({
                url: '/agent_ac/pnrtickets/'+master_id,
                type: 'GET',
                data: $(this).serialize(),
                // data: $(this).serialize(),
                 dataType:'html',
                success: function(response)
                {
                    // console.log(response);
                    $("#result").html(response);
                }
            }); 

        });  
        
    </script>
@endsection