@extends('backend.admin_master')

@section('contents')
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('/dashboard') }}">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Agent Summary</a>
            </li>
        </ul>

        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <a href="{{ url('') }}" class="btn btn-info"><i class="fa fa-plus"></i> New</a>
            </div>
        </div>
    </div>
    <h3 class="page-title"> List of All Agents with Summaries
    </h3>
    
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>Agents </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                    </div>
                </div>
                <div class="portlet-body flip-scroll">
                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="agentSummary">
                        <thead class="flip-content">
                            <tr>
                                <th width="10%"> S.N.</th>
                                <th>Agent Name</th>
								<th>Currency</th>
								<th>Organisation Name</th>
								<th>Options</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            
                            @foreach($ac_agents as $key => $ac_agent)
                            <tr>
                                <td>{{ ++$key }}</td>
								<td>{{ $agent_detail->getAgentName($ac_agent->user_id)}}</td>
								<td>{{ $ac_agent->billing_currency}}</td>
								<td>{{ $ac_agent->org}}</td>
                                <td>
                                <div class="col-md-12" style="display:flex;">
                                <a href="{{route('agent_ac.agentallpnrs',$ac_agent->user_id)}}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody> 
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer')
    <script>
        $("#agentSummary").DataTable();
    </script>
@endsection