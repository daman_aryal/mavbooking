 <table class="table table-striped table-bordered table-hover dt-responsive" width="100%">
    <thead class="flip-content">
        <tr>
            <th width="10%"> S.N.</th>
			<th>Ticket #</th>
			<th>Passenger Name</th>
            <th>Flight Amount</th>
            <th>Tax Amount</th>
        </tr>
    </thead>
    
    <tbody>
        @foreach($pnr_tickets as $key => $pnr_ticket)
        <tr class="tr">
            <td>{{ ++$key }}</td>
			<td>{{$pnr_ticket->ticket->ticket_no}}</td>
            <td>{{$passenger_name->getPassengerN($pnr_ticket->passenger_id)}}</td>
            <td>{{$currency->getCurrency($pnr_ticket->agent_id)}}{{$pnr_ticket->amount}}</td>
            <td>{{$currency->getCurrency($pnr_ticket->agent_id)}}{{$pnr_ticket->tax_amount}}</td>
        </tr>
        @endforeach
    </tbody> 
</table>