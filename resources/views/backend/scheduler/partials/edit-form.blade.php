<div class="form-body row">
  <div class="form-group col-md-3">
      <label>Date : {{$scheduler->date}}</label>
  </div>
  <div class="form-group col-md-3">
    <label class="control-label">Depart time : {{$scheduler->depart_time}}</label>
  </div>
  <div class="form-group col-md-3">
    <label class="control-label">Arrive time : {{$scheduler->arrive_time}}</label>
  </div>
  <div class="form-group col-md-4">
      <label><strong>Flight no : {{$city->getCity($scheduler->flight->source_id)}}-{{$city->getCity($scheduler->flight->destination_id)}}-{{$scheduler->flight->flight_no}}</strong></label>
  </div>
  <div class="clearfix"></div>
  <div class="form-group col-md-3">
      <label>Cabin : {{$cabin->getCabin($scheduler->cabin_id)}}</label>
  </div>
  <div class="clearfix"></div>
  <div class="form-group col-md-3">
      <label>RBD : {{$rbd->getRbd($scheduler->rbd_id)}}</label>
  </div>
  <div class="clearfix"></div>
  <div class="form-group col-md-2">
      <label>Seat</label>
      <div class="input-group">
          <span class="input-group-addon">
              <i class="fa fa-pencil"></i>
          </span>
       {!! Form::text('seat',old('seat'),  ['class' => 'form-control']) !!}
      </div>
  </div>
  <div class="clearfix"></div>
  <div class="form-group col-md-2">
      <label>Price OneWay(NPR)</label>
      <div class="input-group">
          <span class="input-group-addon">
              <i class="fa fa-pencil"></i>
          </span>
       {!! Form::text('price_local_oneway',old('price_local_oneway'),  ['class' => 'form-control']) !!}
      </div>
  </div>
  <div class="form-group col-md-2">
      <label>Price OneWay(USD)</label>
      <div class="input-group">
          <span class="input-group-addon">
              <i class="fa fa-pencil"></i>
          </span>
       {!! Form::text('price_usd_oneway',old('price_usd_oneway'),  ['class' => 'form-control']) !!}
      </div>
  </div>
  <div class="form-group col-md-2">
      <label>Price Roundtrip(NPR)</label>
      <div class="input-group">
          <span class="input-group-addon">
              <i class="fa fa-pencil"></i>
          </span>
       {!! Form::text('price_local_roundtrip',old('price_local_roundtrip'),  ['class' => 'form-control']) !!}
      </div>
  </div>
  <div class="form-group col-md-2">
      <label>Price Roundtrip(USD)</label>
      <div class="input-group">
          <span class="input-group-addon">
              <i class="fa fa-pencil"></i>
          </span>
       {!! Form::text('price_usd_roundtrip',old('price_usd_roundtrip'),  ['class' => 'form-control']) !!}
      </div>
  </div>
  <div class="form-actions col-md-12">
        <button type="submit" class="btn blue">Update</button>
  </div>
</div>