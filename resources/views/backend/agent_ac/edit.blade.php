@extends('backend.admin_master')
 
@section('contents')
	<div class="row">
	    <div class="col-lg-12 margin-tb">
	        <div class="pull-left">
	            <h2>Edit Role</h2>
	        </div>
	        <div class="pull-right">
	            <a class="btn btn-primary" href="{{ url('/role/edit') }}"> Back</a>
	        </div>
	    </div>
	</div>
	
	{!! Form::model($role, ['url' => ['role/update', $role->id]]) !!}
		@include('backend.roles.form')
	{!! Form::close() !!}
@endsection