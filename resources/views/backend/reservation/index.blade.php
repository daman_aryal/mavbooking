@extends('backend.admin_master')

@section('headerscript')
<style type="text/css">
    .btn-group ul li a:hover{
        background: #217ebd;
        color:#FFF;
    }
</style>
@endsection

@section('contents')
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('/dashboard') }}">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Reservation</a>
            </li>
        </ul>

        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <a href="{{ url('reservation/create')}}" class="btn btn-info"><i class="fa fa-plus"></i> New</a>
            </div>
        </div>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> List of All Reservations
    </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>Reservations </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                    </div>
                </div>
                <div class="portlet-body flip-scroll">
                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="reservations">
                        <thead class="flip-content">
                            <tr>
                                <th width="20%"> S.N.</th>
                                <th>PNR</th>
                                <th>Options</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            @if(count($ticket) > 0)
                                @foreach($ticket as $t)
                                <tr>
                                    <td>{{$t->id}}</td>
                                    <td>{{$t->pnr}}</td>
                                    <td style="width:180px">
                                    <div class="btn-group">
                                        <button class="btn blue dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action
                                            <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu" style="position: relative;">
                                            <li>
                                                <a href="{{ url('reservation/no/show/'.$t->pnr)}}"> No Show </a>
                                            </li>
                                            <li>
                                                <a href="{{ url('/datechange/'.$t->pnr) }}">Date Change </a>
                                            </li>
                                            <li>
                                                <a href="{{ url('reservation/cancle/'.$t->pnr) }}"> Cancellation </a>
                                            </li>
                                        </ul>
                                    </div>

                                    </td>
                                </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer')
    <script>
        $("#reservations").DataTable();
    </script>
@endsection