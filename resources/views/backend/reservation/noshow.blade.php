@extends('backend.admin_master')

@section('headerscript')
<style type="text/css">
    .btn-group ul li a:hover{
        background: #217ebd;
        color:#FFF;
    }
</style>
@endsection

@section('contents')
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ url('/dashboard') }}">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Reservation</a>
            </li>
        </ul>

        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <a href="{{ url('reservation/create') }}" class="btn btn-info"><i class="fa fa-plus"></i> New</a>
            </div>
        </div>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> PNR No : <strong>{{$pnr_no}}</strong>
    </h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>Reservations </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                    </div>
                </div>
                <div class="portlet-body flip-scroll">
                    <form action="{{ url('reservation/refund') }}" method="POST">
                        {{csrf_field()}}
                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="reservations">
                            <thead class="flip-content">
                                <tr>
                                    <th width="20%"> S.N.</th>
                                    <th>Ticket No</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                @foreach($ticket as $t)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$t->id}}</td>

                                    @if($t->status == 0)
                                        <td>Normal</td>
                                        <td>
                                            <div class="btn-group">
                                              <input type="checkbox" id="{{$i}}" name="noshow"> 
                                              <label for="{{$i}}">No show</label>
                                            </div>
                                        </td>
                                    @endif                                
                                    @if($t->status == 2)
                                        <td>No show</td>
                                        <td>
                                            Not seen
                                        </td>
                                    @endif
                                </tr>
                                @endforeach 
                            </tbody>
                        </table>
                        <hr>
                        <div class="form-group">
                            <label for="">Penalty</label>
                            <input type="text" class="form-control" name="penalty">    
                        </div>
                        <input type="hidden" value="{{$t->pnr}}" name="pnr">
                        
                        <input type="submit" class="btn btn-info">
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer')
    <script>
        $("#reservations").DataTable();
    </script>
@endsection