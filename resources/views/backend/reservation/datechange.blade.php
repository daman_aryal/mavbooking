@extends('backend.admin_master')
@section('contents')
	<!-- BEGIN PAGE BAR -->
	<div class="page-bar">
	    <ul class="page-breadcrumb">
	        <li>
	            <a href="{{ url('/dashboard') }}">Home</a>
	            <i class="fa fa-circle"></i>
	        </li>
	        <li>
	            <a href="{{ url('reservation/') }}">Reservation</a>
	            <i class="fa fa-circle"></i>
	        </li>
	        <li>
	            <a href="#">Edit-DateChange</a>
	        </li>
	    </ul>
		   <div class="page-toolbar">
	            <div class="btn-group pull-right">
	                <a href="{{ url('reservation/') }}" class="btn btn-info"><i class="fa fa-arrow-circle-left"></i> Back</a>
	            </div>
	        </div>
	</div>
	<h3 class="page-title">Change Date of Flight </h3>
	<!-- END PAGE BAR -->

	<div class="row">
  		<div class="col-md-12">
		


        <!-- BEGIN SAMPLE FORM PORTLET-->
	        <div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-red-sunglo">
					    <i class="fa fa-user-plus"></i>
					    <span class="caption-subject bold uppercase"> Change Date of this Reservation</span>
					</div>
				</div>

	            <div class="portlet-body">
	                <div class="form-body">
		                {{-- sdakljf --}}
		                <div class="portlet-body flip-scroll">
			                <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="reservations">
		                        <thead class="flip-content">
		                            <tr>
		                                <th>From</th>
										<th>To</th>
										<th>Flight no</th>
										<th>Class</th>
										<th>Price</th>
										<th class="thead">Date</th>
									</tr>
		                        </thead>
		                        <tbody>
		                        @foreach($scheduler_in_ticket as $s)
		                        		<form class="date_change_form">
											<tr>
			                        			<td>{{$s->source}}</td>
			                        			<td>{{$s->cabin->cabin}}</td>
			                        			<td>
			                        				@if(count($scheduler_in_ticket) > 1)
			                        					{{$s->roundtrip_price_mode($ticket_first->agent_id, $ticket_first->id)}}
			                        				@else
			                        					{{$s->oneway_price_mode($ticket_first->agent_id, $ticket_first->id)}}
			                        				@endif
			                        			</td>
			                        			<td class="tbody">
			                        				<div class="form-group">
			                                           	<div class="input-group date date_change" data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
			                                                <input type="text" name="date_change" class="form-control" value="{{$s->date}}">
			                                                <span class="input-group-btn">
			                                                    <button class="btn default datepicker_btn" type="button">
			                                                        <i class="fa fa-calendar"></i>
			                                                    </button>
			                                                </span>
			                                            </div>
			                                        </div>
												</td>
											</tr>
			                        		<tr>
			                        			<input type="hidden" value="{{$s->source}}" name="source">
												<input type="hidden" value="{{$s->destination}}" name="destination">
												<input type="hidden" value="{{$s->flight_id}}" name="flight_id">
												<input type="hidden" value="{{$s->cabin_id}}" name="cabin">
												<input type="hidden" value="{{$billing_currency}}" name="billing_currency">
												<input type="hidden" value="{{$ticket_first->agent_id}}" class="agent_id">
												<input type="hidden" value="{{$ticket_first->pnr}}" class="pnr_no">
												<input type="hidden" value="{{$ticket_first->tax_amount}}" class="tax_amount">
												<td colspan="7" hidden>
													<input type="text" class="form-control penalty" placeholder="Penalty [RS or USD]">
													<hr>
			                        				<ul class="ticket_list">
			                        					@foreach($tickets_as_pnr as $t_as_pnr)
			                        						<?php $i++ ?>
			                        						<li>
			                        							<div class="pull-left ticket_passenger">
		                        									<input type="checkbox" value="{{$t_as_pnr->passenger->id}}" id="ticket_no{{$i}}" class="passenger_id" name="passenger_id[]"> 
		                        								</div>
																<div class="pull-right">
																	<label for="ticket_no{{$i}}">
																		Ticket no : 
																		<strong>{{$t_as_pnr->ticket_no}}</strong>
																			[<strong>
																		{{$t_as_pnr->passenger->fname." ".$t_as_pnr->passenger->mname." ".$t_as_pnr->passenger->lname}}
																		</strong>]
																	</label>
																</div>
															</li>
															<br>
				                        				@endforeach
			                        				</ul>
			                        			</td>
			                        		</tr>
			                        		<tr>
			                        			<td colspan="7">
			                        				<input type="submit" value="Check Availability" class="btn btn-info book_btn">
			                        				<div class="cancel pull-right" hidden>
			                        					<button class="btn btn-danger">Cancel and change date</button>	
			                        				</div>
			                        			</td>
			                        		</tr>
			                        	</form>
		                        	@endforeach()
		                        </tbody>
		                    </table>
                		</div>
					</div>
        		</div>
	        </div>
        </div>
   	</div>
@stop
@section('footer')
<script type="text/javascript">
	/**
	 * initial for date picket
	 */
	var date_chage = $(".date_change").datepicker();
	$(".date_change_form").on('submit', function(e){
		e.preventDefault();
		var that = $(this);
		var data = that.serialize();
		$.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});
		
		$.post('/reservation/check/availabiltiy', data, function(result){
			if(result !== 0){
				that.next('tr').next('tr').find('td').attr("hidden", false);
				that.next('tr').next('tr').next('tr').find("input[type='submit']").attr('value', 'Book');
				that.next('tr').next('tr').next('tr').find(".cancel").attr("hidden", false);

				$(".date_change_form").on("submit", function(){
					var store_checkbox = [];
					$(".passenger_id:checked").each(function(key, value){
						store_checkbox.push($(value).val());								
					});

					$.post('/reservation/datechange/reserve', { 'agent_id' : $(".agent_id").val() ,'scheduler_id' : result.scheduler_id, 'price':result.price, 'passenger' : store_checkbox, 'pnr_no' : $(".pnr_no").val(), 'penalty' : $(".penalty").val(), 'tax_amount' : $(".tax_amount").val()}, function(result){
						console.log(result);
					})

				});
				
				if(that.next('tr').next('tr[class="not_available"]').length < 1){
					$(`<tr class="not_available">
						<td colspan="7">
							<div class="alert alert-success">
								<strong>Select Passengers.</strong>
							</div>
						</td>
					</tr>`).insertAfter(that.next('tr'));
				}
			}else{
				if(that.next('tr').next('tr[class="not_available"]').length < 1){
					$(`<tr class="not_available">
						<td colspan="7">
							<div class="alert alert-danger">
							  Flight <strong>Not available</strong></a>.
							</div>
						</td>
					</tr>`).insertAfter(that.next('tr'));
				}
			}
		});
	});


	$('#tableSelect tbody tr').click(function(){
        // console.log('clicked');
        if (($('#tableSelect tbody tr').find('td.checkradio').find('span.checked')).length > 0) {
          console.log('if here');
          $('#tableSelect tbody tr').find('td.checkradio').find('span.checked').removeClass('checked');
          $(this).find('td.checkradio').find('span').addClass('checked');
        }
        else {
          // console.log('else here');
          $(this).find('td.checkradio').find('span').addClass('checked');
        }
      });

      $('#tableSelect2 tbody tr').click(function(){
        // console.log('clicked');
        if (($('#tableSelect2 tbody tr').find('td.checkradio').find('span.checked')).length > 0) {
          // console.log('if here');
          $('#tableSelect2 tbody tr').find('td.checkradio').find('span.checked').removeClass('checked');
          $(this).find('td.checkradio').find('span').addClass('checked');
        }
        else {
          // console.log('else here');
          $(this).find('td.checkradio').find('span').addClass('checked');
        }
      });
</script>  
@endsection