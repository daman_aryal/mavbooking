<div class="row">  
  <div class="col-md-6">
          <!-- BEGIN SAMPLE FORM PORTLET-->
      <div class="portlet light bordered">
        <div class="portlet-body form">
            <form>
              <div class="form-body">
            	  <div class="form-group">
  			         <label for="meal_name">Meal Preference:</label>
  			         <div class="input-group">
  			    	    <input class="form-control" type="text" name="meal_name" id="meal_name" required="">
  			  	      </div>
  			        </div>

                <div class="form-actions">
                  <button type="submit" class="btn btn-primary">Save</button>    
                  <a href="{{url('meal')}}" class="btn btn-default">Cancel</a>
                </div>
              </div>
            </form>
          </div>
      </div>
  </div>
</div>
