@extends('backend.admin_master')
 
@section('contents')
	<div class="row">
	    <div class="col-lg-12 margin-tb">
	        <div class="pull-left">
	            <h2>Edit Agent Registration</h2>
	        </div>
	        <div class="pull-right">
	            <a class="btn btn-primary" href="{{ url('agent/') }}"> Back</a>
	        </div>
	    </div>
	</div>
	
	{!! Form::model($agent, ['url' => ['agent/update', $agent->user_id]]) !!}
		@include('backend.agent.form')
	{!! Form::close() !!}
@endsection