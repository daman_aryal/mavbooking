@extends('backend.admin_master')

@section('contents')
	<div class="page-bar">
	    <ul class="page-breadcrumb">
	        <li>
	            <a href="index.html">Home</a>
	            <i class="fa fa-circle"></i>
	        </li>
	    </ul>
	    
	</div>
	<!-- END PAGE BAR -->
	<!-- BEGIN PAGE TITLE-->
	<h3 class="page-title"> Dashboard
	    
	</h3>
	<div class="row">
        <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat blue">
                <div class="visual">
                    <i class="fa fa-comments"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{$flights}}">0</span>
                    </div>
                    <div class="desc"> Total Flights</div>
                </div>
                <!-- <a class="more" href="javascript:;"> View more
                    <i class="m-icon-swapright m-icon-white"></i>
                </a> -->
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat red">
                <div class="visual">
                    <i class="fa fa-bar-chart-o"></i>
                </div>
                <div class="details">
                    <div class="number">
                        NPR <span data-counter="counterup" data-value="{{$tot_collection_npr}}">0</span></div>
                    <div class="desc"> Total NPR Collections</div>
                </div>
               <!--  <a class="more" href="javascript:;"> View more
                    <i class="m-icon-swapright m-icon-white"></i>
                </a> -->
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat red">
                <div class="visual">
                    <i class="fa fa-bar-chart-o"></i>
                </div>
                <div class="details">
                    <div class="number">
                        USD <span data-counter="counterup" data-value="{{$tot_collection_usd}}">0</span></div>
                    <div class="desc"> Total USD Collections</div>
                </div>
               <!--  <a class="more" href="javascript:;"> View more
                    <i class="m-icon-swapright m-icon-white"></i>
                </a> -->
            </div>
        </div>
        <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat green">
                <div class="visual">
                    <i class="fa fa-shopping-cart"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{$tot_booking}}">0</span>
                    </div>
                    <div class="desc"> Total Bookings </div>
                </div>
               <!--  <a class="more" href="javascript:;"> View more
                    <i class="m-icon-swapright m-icon-white"></i>
                </a> -->
            </div>
        </div>
        <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat purple">
                <div class="visual">
                    <i class="fa fa-globe"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{$tot_agents}}"></span></div>
                    <div class="desc"> Total Agents</div>
                </div>
                <!-- <a class="more" href="javascript:;"> View more
                    <i class="m-icon-swapright m-icon-white"></i>
                </a> -->
            </div>
        </div>
    </div>
    <div class="row">
    	<div class="col-md-8 col-sm-8">
            <div class="portlet light bordered">
		    	<div class="portlet-title">
		            <div class="caption">
		                <i class="icon-check font-blue"></i>
		                <span class="caption-subject font-blue bold uppercase">Recent Reservations</span>
		            </div>
		        </div>    
		    	<div class="portlet-body">
		            <div class="scroller" style="height: auto;" data-always-visible="1" data-rail-visible="0">
		                <ul class="feeds">
		                    <li>
                        	@foreach($tickets as $key=>$ticket)
                        		@foreach(($model_ticket->scheduler_ticket_pivot($ticket->id)) as $test)
			                        <div class="col1">
			                            <div class="cont">
			                                <div class="cont-col1">
			                                    <div class="label label-sm label-info">
			                                        <i class="fa fa-check"></i>
			                                    </div>
			                                </div>

			                                <div class="cont-col2">
	                                    		<div class="desc"> {{$model_ticket->getAgent($ticket->agent_id)}} has booked for Flight No.:{{$model_ticket->getFlightno($test->flight_id)}} [{{$test->source}}-{{$test->destination}}]<br>	 	<em>Ticket No:{{$ticket->ticket_no}}</em>.
	                                    		</div>
			                                    		
			                                </div>
			                            </div>
			                        </div>
			                        <div class="col2">
			                            <div class="date"> {{$ticket->created_at->toDateString()}} </div>
			                        </div>
                            	@endforeach
                            @endforeach
		                    </li>
		                </ul>
		            </div>
		            <div class="scroller-footer">
		                <div class="btn-arrow-link pull-right">
		                    <a href="javascript:;">See All Records</a>
		                    <i class="icon-arrow-right"></i>
		                </div>
		            </div>
		        </div>
			</div>
		</div>  
		<div class="col-md-4 col-sm-4">
            <div class="portlet light bordered">
		    	<div class="portlet-title">
		            <div class="caption">
		                <i class="icon-plane font-blue"></i>
		                <span class="caption-subject font-blue bold uppercase">Today's Flights</span>
		            </div>
		        </div>    
		    	<div class="portlet-body">
		    		<div class="table-scrollable table-scrollable-borderless">
                        <table class="table table-hover table-light">
                            <thead>
                                <tr class="uppercase">
                                    <th> # </th>
                                    <th> Flight No </th>
                                    <th> Departure </th>
                                    <th> Arrival </th>
                                </tr>
                            </thead>
                            <tbody>

                            	@foreach($today_flights as $key=>$flight)
                                <tr>
                                    <td> {{++$key}} </td>
                                    <td> {{$flight->flight->flight_no}} </td>
                                    <td> {{$flight->depart_time}}</td>
                                    <td> {{$flight->arrive_time}} </td>
                                </tr>
                                @endforeach
                                
                       
                            </tbody>
                        </table>
                    </div>
		        </div>
			</div>
		</div>       
    </div>
@endsection


