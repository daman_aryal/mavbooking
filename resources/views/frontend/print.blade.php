<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Report</title>
	{{ Html::style('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}
	
	<style>
		.underline{
			color: #169bc1;
			font-weight: bold;
			border-bottom: 2px solid #169bc1;
			width: 100%;
		}
		.border{
			border:1px solid #169bc1 ;
			margin-top: 5px;
		}
		.top-border{
			border-top: 1px solid #c2c6c7;
		}
		.from-to{
			color: #169bc1;
		}
		.green-font{
			color: #009900;
			font-weight: bold;
		}
	</style>
</head>
<body>
	<div class="container">
		<div class="row">
			<h4>Booking reservation number: <span style="color: red">6J4FG6</span></h4>
		</div>
		<div class="row">
			<h5 class="underline">TRAVELER INFORMATION</h5>
		</div>
		
		@foreach($passenger_ticket as $t)

		<div class="row border">
			<div class="col-md-4">
				
				<h5>Other phone: </h5>
			</div>
			<div class="col-md-8">
				<h5 style="margin-top: 37px">KTM GORKHA TRAVELS 4440761 REF NAYAN</h5>
			</div>
			<hr>
			<div class="col-md-12 top-border">
			
			<h4>{{$scheduler_ticket->flight->source->city_name}} - {{$scheduler_ticket->flight->destination->city_name}}</h4>
			</div>
			<div class="col-md-4 top-border">
				<h5>E-ticket number</h5>
				<h5>Issuing Airline:</h5>
				<h5>Ticket status: e-ticket processed 06FEB13</h5>
			</div>
			<div class="col-md-8 top-border">
				<h5>{{$t->ticket_no}}</h5>
				<h5>MI</h5>
			</div>
		</div>
		@endforeach
			
		<div class="row">
			<h5 class="underline">FLIGHT INFORMATION</h5>
		</div>
			
		<div class="row border">
				<div class="col-md-12">
					<h3 class="from-to">
						{{$scheduler_ticket->flight->source->city_name}} - {{$scheduler_ticket->flight->destination->city_name}}
					</h3>
				</div>
				<div class="col-md-12">
					<h5>Singapore Airlines | SQ 5311 <span class="green-font"> confirmed</span></h5>
				</div>
				<div class="col-md-12 top-border">
					<h4><strong>{{Carbon\Carbon::parse($scheduler_ticket->date)->format('d M, Y')}}</strong> | duration 4:55</h4>
				</div>
				<div class="col-md-3">
					<h5><strong>Dep: {{$scheduler_ticket->depart_time}}</strong></h5>
					<h5><strong>Arr: {{$scheduler_ticket->arrive_time}}</strong></h5>
				</div>
				<div class="col-md-9">
					<h5><strong>{{$scheduler_ticket->flight->source->city_name}} </strong> | 
					{{$scheduler_ticket->flight->source->airport_name}}</h5>
					<h5><strong>{{$scheduler_ticket->flight->destination->city_name}}</strong> | 
					{{$scheduler_ticket->flight->destination->airport_name}}</h5>
				</div>
				<div class="col-md-4 top-border">
					<h5>Confirmation Number:</h5>
					<h5>Flight Number:</h5>
					<h5>Fare type:</h5>
					<h5>Operated by:</h5>
					<h5>Aircraft:</h5>
					<h5>Meal:</h5>
					<h5>Baggage:</h5>
					<h5>Last check in:</h5>
				</div>
				<div class="col-md-8 top-border">
					<h5><strong>Singapore Airlines RMKSE4</strong></h5>
					<h5><strong>Himalaya Airlines - {{$scheduler_ticket->flight->flight_no}}</strong></h5>
					<h5><strong>Coach</strong></h5>
					<h5><strong>SilkAir</strong></h5>
					<h5><strong>Airbus Industrie A320-100/200</strong></h5>
					<h5>{{$scheduler_ticket->meal_id}}</h5>
					<h5>Baggage:</h5>
					<h5>Last check in:</h5>
				</div>
				<div class="col-md-4 top-border">
					<h5>Indiver Badal</h5>
					<h5>Meal preferences: </h5>
				</div>
				<div class="col-md-8 top-border">
					<h5 style="margin-top: 37px">Standard</h5>
				</div>
				<div class="col-md-4 top-border">
					<h5>Kabindramr Shrestha</h5>
					<h5>Meal preferences:</h5>
				</div>
				<div class="col-md-8 top-border">
					<h5 style="margin-top: 37px">Standard</h5>
				</div>
		</div>
		
		<div class="row">
			<h5>CONDITIONS OF CONTRACT AND OTHER IMPORTANT NOTICES</h5>
			<p>
				PASSENGERS ON A JOURNEY INVOLVING AN ULTIMATE DESTINATION OR A STOP IN A COUNTRY OTHER THAN THE COUNTRY OF
				DEPARTURE ARE ADVISED THAT INTERNATIONAL TREATIES KNOWN AS THE MONTREAL CONVENTION, OR ITS PREDECESSOR, THE
				WARSAW CONVENTION, INCLUDING ITS AMENDMENTS (THE WARSAW CONVENTION SYSTEM), MAY APPLY TO THE ENTIRE JOURNEY,
				INCLUDING ANY PORTION THEREOF WITHIN A COUNTRY.  FOR SUCH PASSENGERS, THE APPLICABLE TREATY, INCLUDING SPECIAL
				CONTRACTS OF CARRIAGE EMBODIED IN ANY APPLICABLE TARIFFS, GOVERNS AND MAY LIMIT THE LIABILITY OF THE
				CARRIER.CONVENTION SYSTEM), MAY APPLY TO THE ENTIRE JOURNEY, INCLUDING ANY PORTION THEREOF WITHIN A COUNTRY. FOR
				SUCH PASSENGERS, THE APPLICABLE TREATY, INCLUDING SPECIAL CONTRACTS OF CARRIAGE EMBODIED IN ANY APPLICABLE TARIFFS,
				GOVERNS AND MAY LIMIT THE LIABILITY OF THE CARRIER.
			</p>
			<span>http://www.iatatravelcentre.com/tickets</span>
			<h5 style="text-align: center;">CHECKMYTRIP</h5>
			<h5 style="text-align: center;">© 2011 Amadeus IT Group S.A. All Rights Reserved</h5>
		</div>
	</div>
</body>
</html>
