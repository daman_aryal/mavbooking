@extends('layouts.app')
@section('headerscript')
        <!-- BEGIN THEME GLOBAL STYLES -->
        {!! Html::style('assets/global/css/components.min.css') !!}
        {!! Html::style('assets/global/css/plugins.min.css') !!}
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
       {{--  {!! Html::style('assets/layouts/layout3/css/layout.min.css') !!}
        {!! Html::style('assets/layouts/layout3/css/themes/default.min.css') !!}
        {!! Html::style('assets/layouts/layout3/css/custom.min.css') !!} --}}
        {!! Html::style('css/owl.theme.css') !!}
        {!! Html::style('css/owl.carousel.css') !!}
        {!! Html::style('css/custom.css') !!}


        {{-- custom css --}}
        {!! Html::style('css/typehead.css') !!}

@endsection
@section('main')
<!-- BEGIN HEADER -->
@include('backend.partial.header')   
<!-- END HEADER -->
<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"> </div>
<!-- END HEADER & CONTENT DIVIDER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">

    <!-- BEGIN SIDEBAR -->
     @include('frontend.partial.sidebar')
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">

        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            @include('backend/city/partials/_message'){{-- for session msg flash --}}
            @yield('contents')    
        </div>
        <!-- END CONTENT BODY -->
    </div>
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
@include('backend.partial.footer')

<!-- END FOOTER -->

@endsection
@section('footer')

        <!-- BEGIN PAGE LEVEL PLUGINS -->
{{--         {!! Html::script('assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') !!}
        {!! Html::script('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') !!} --}}
        {!! Html::script('assets/global/plugins/clockface/js/clockface.js') !!}
        {{-- {!! Html::script('assets/global/plugins/morris/raphael-min.js') !!} --}}
{{--         {!! Html::script('assets/global/plugins/amcharts/amcharts/amcharts.js') !!}
        {!! Html::script('assets/global/plugins/amcharts/amcharts/serial.js') !!}
        {!! Html::script('assets/global/plugins/amcharts/amcharts/pie.js') !!}
        {!! Html::script('assets/global/plugins/amcharts/amcharts/radar.js') !!}
        {!! Html::script('assets/global/plugins/amcharts/amcharts/themes/light.js') !!}
        {!! Html::script('assets/global/plugins/amcharts/amcharts/themes/patterns.js') !!}
        {!! Html::script('assets/global/plugins/amcharts/amcharts/themes/chalk.js') !!}
        {!! Html::script('assets/global/plugins/amcharts/ammap/ammap.js') !!}
        {!! Html::script('assets/global/plugins/amcharts/ammap/maps/js/worldLow.js') !!}
        {!! Html::script('assets/global/plugins/amcharts/amstockcharts/amstock.js') !!} --}}
{{--         {!! Html::script('assets/global/plugins/fullcalendar/fullcalendar.min.js') !!}
        {!! Html::script('assets/global/plugins/flot/jquery.flot.min.js') !!}
        {!! Html::script('assets/global/plugins/flot/jquery.flot.resize.min.js') !!}
        {!! Html::script('assets/global/plugins/flot/jquery.flot.categories.min.js') !!}
        {!! Html::script('assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') !!}
        {!! Html::script('assets/global/plugins/jquery.sparkline.min.js') !!} --}}
        <!-- END PAGE LEVEL PLUGINS -->


        {{-- type ahead --}}
        {!! Html::script('assets/global/plugins/typeahead/handlebars.min.js') !!}
        {{-- {!! Html::script('assets/global/plugins/typeahead/typeahead.bundle.min.js') !!} --}}
        {{-- End of type adhead --}}       

         <!-- BEGIN PAGE LEVEL SCRIPTS -->
        {!! Html::script('assets/pages/scripts/components-date-time-pickers.min.js') !!}
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
{{--         {!! Html::script('assets/layouts/layout3/scripts/layout.min.js') !!}
        {!! Html::script('assets/layouts/layout3/scripts/demo.min.js') !!}
        {!! Html::script('assets/layouts/global/scripts/quick-sidebar.min.js') !!} --}}
        {!! Html::script('js/owl.carousel.min.js') !!}
        {!! Html::script('js/custom.js') !!}



        {!! Html::script('assets/global/plugins/select2/js/select2.full.min.js') !!}
        {!! Html::script('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') !!}
        {!! Html::script('assets/global/plugins/jquery-validation/js/additional-methods.min.js') !!}
        {!! Html::script('assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js') !!}
        {!! Html::script('assets/pages/scripts/form-wizard.min.js') !!}
        {!! Html::script('js/typeahead.bundle.min.js') !!}

        @yield('footerscripts')
@endsection