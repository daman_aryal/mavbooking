<div class="page-header">
    <!-- BEGIN HEADER TOP -->
    <div class="page-header-top">
        <div class="container">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <a href="{{ url('agent/dashboard') }}">
                    <img src="{{ url('img/logo.png') }}" alt="logo" class="logo-default img-responsive logo-size">
                </a>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler"></a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                   
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <li style="margin-top: 16px;">
                        
                        <span class="username username-hide-mobile">Credit NPR 1,20,000.00</span>
                    </li>
                    <li class="dropdown dropdown-user dropdown-dark">
                        
                            {{-- <img alt="" class="img-circle" src="{{ url('assets/layouts/layout3/img/avatar9.jpg') }}"> --}}
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a href="{{ url('/logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                             <i class="icon-key"></i> Log Out 
                                </a>

                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
    </div>
    <!-- END HEADER TOP -->
    <!-- BEGIN HEADER MENU -->
    <div class="page-header-menu">
        <div class="container">
            <!-- END HEADER SEARCH BOX -->
            <!-- BEGIN MEGA MENU -->
            <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
            <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
            <div class="hor-menu  ">
                <ul class="nav navbar-nav">
                    <li class="menu-dropdown active">
                        <a href="{{ url('agent/dashboard') }}"> Home
                            <span class="arrow"></span>
                        </a>
                    </li>
                    <li class="menu-dropdown">
                        <a href="javascript:;"> Bookings
                            <span class="arrow"></span>
                        </a>
                    </li>
                    <li class="menu-dropdown">
                        <a href="javascript:;"> Account Balance
                            <span class="arrow"></span>
                        </a>
                    </li>
                    </li>
                </ul>
            </div>
            <!-- END MEGA MENU -->
        </div>
    </div>
    <!-- END HEADER MENU -->
</div>