@extends('frontend.master')
@section('contents')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="index.html">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Search</span>
        </li>
    </ul>
</div>
<div class="row" style="margin-top:20px;">
    <div class="col-md-12 col-lg-12">
        {{ Form::open([ 'url' => '/agent/search']) }}
            <div class="portlet light bordered">
                <div class="row" style="padding-left:20px">
                    <div class="form-group">
                        <div class="md-radio-inline">
                            <div class="md-radio">
                                <input type="radio" id="radio1" name="trip" class="md-radiobtn" value="one" @if ($request->trip == 'one') checked @endif>
                                                <label for="radio1">
                                                    <span class="inc"></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> One Way </label>
                            </div>
                            <div class="md-radio">
                                <input type="radio" id="radio2" name="trip" class="md-radiobtn" value="round" @if ($request->trip == 'round') checked @endif>
                                                <label for="radio2">
                                                    <span class="inc"></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> Round Trip </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 col-lg-2">
                        <div class="form-group">
                            <label>From:</label>
                            <div id="remote">
                                <input type="text" name="source" class="form-control typeaheadSource" placeholder="Origin" value="{{$request->source}}">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2 col-lg-2">
                        <div class="form-group">
                            <label>To:</label>
                            <div id="remote">
                                <input type="text" name="destination" class="form-control typeaheadDestination" placeholder="Destination" value="{{$request->destination}}">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-lg-3">
                        <div class="form-group">
                            <label>Dates:</label>
                            <div class="input-group input-large date-picker input-daterange search-date" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                                <input type="text" class="form-control" name="fromDate" value="{{$request->fromDate}}">
                                <span class="input-group-addon"> to </span>
                                <input type="text" class="form-control" name="toDate" value="{{$request->toDate}}">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2 col-lg-2">
                        <div class="form-group">
                            <label>Class:</label>
                            <select class="form-control" name="class">
                            @foreach($cabin as $cabins)
                                <option value="{{$cabins->id}} @if ($request->class == $cabins->id) selected @endif">{{$cabins->cabin}}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>



                    <div class="col-md-3 col-lg-2">
                        <div class="form-group">
                            <label>Person:</label>
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="$request->person">
                                        <span class="glyphicon glyphicon-minus"></span>
                                    </button>
                                </span>
                                <input type="text" name="quant" class="form-control input-number search-no" value="{{$request->person}}" min="1" max="10">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="$request->person">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2 col-lg-1 search_box">
                        <div class="form-group" style="margin-top:4px;">
                            <label></label>
                            {{-- <input type="submit" class="btn btn-xs search_btn" name="search_btn" value="Search"> --}}
                            <button type="submit" class="btn btn-sm search_btn">Search</button>
                        </div>
                    </div>
                </div>
            </div>
        {{Form::close()}}
    </div>
</div>


<div class="row" style="margin-top: 20px">
    <div class="col-md-12">
        <div class="portlet light" id="form_wizard_1">
            <div class="portlet-body form">
                <div class="tab-pane" id="tab4">
                    <p>Ticket <strong>ID 789-7876548765</strong> has been Created and emailed to agent@mavorion.com</p>
                    <br>
                    <a href="{{ url('/agent/dashboard') }}" class="btn book-btn book-btn-info " style="width:171px !important;">Book Another Flight</a>

                    <a href="{{ url('/print') }}" class="btn book-btn book-btn-info " target="_blank">Print Ticket</a>
                </div>
                           
            </div>
        </div>
    </div>
</div>
@endsection


@section('footerscripts')
    
    <script>
      var city = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('city_name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
          url: '/api/agent/city?city_name=%QUERY',
          wildcard: '%QUERY'
        }
      });

      $('.typeaheadSource').typeahead(null, {
      name: 'source',
      display: 'source',
      source: city
      });

      var cityDestination = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('city_name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
          url: '/api/agent/cityDestination?city_name=%QUERY',
          wildcard: '%QUERY'
        }
      });

      $('.typeaheadDestination').typeahead(null, {
      name: 'destination',
      display: 'destination',
      source: cityDestination
      });

    </script>

@endsection



