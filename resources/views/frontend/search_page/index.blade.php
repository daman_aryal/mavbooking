@extends('frontend.master')
@section('contents')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="index.html">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Search</span>
        </li>
    </ul>
</div>

<div class="row" style="margin-top:20px;">
    <div class="col-md-12 col-lg-12">
         {!! Form::open(array('url' => '/reservation/search','method'=>'POST')) !!}
            <div class="portlet light bordered">
                <div class="row" style="padding-left:20px">
                    <div class="form-group">
                        <div class="md-radio-inline">
                            <div class="md-radio">
                                <input type="radio" id="radio1" name="trip" class="md-radiobtn" value="one" @if ($request->trip == 'one') checked @endif>
                                                <label for="radio1">
                                                    <span class="inc"></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> One Way </label>
                            </div>
                            <div class="md-radio">
                                <input type="radio" id="radio2" name="trip" class="md-radiobtn" value="round" @if ($request->trip == 'round') checked @endif>
                                                <label for="radio2">
                                                    <span class="inc"></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> Round Trip </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 col-lg-2">
                        <div class="form-group">
                            <label>From:</label>
                            <div id="remote">
                                <input type="text" id="typeaheadSource" name="source" class="form-control typeaheadSource" placeholder="Origin" value="{{$request->source}}">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2 col-lg-2">
                        <div class="form-group">
                            <label>To:</label>
                            <div id="remote">
                                <input type="text" name="destination" class="form-control typeaheadDestination" placeholder="Destination" value="{{$request->destination}}">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-lg-3">
                        <div class="form-group">
                            <label>Dates:</label>
                            <div class="input-group input-large date-picker input-daterange search-date" data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
                                <input type="text" class="form-control" name="fromDate" value="{{$request->fromDate}}" autocomplete="off">
                                <span class="input-group-addon"> to </span>
                                <input type="text" class="form-control" name="toDate" value="{{$request->toDate}}" autocomplete="off">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2 col-lg-2">
                        <div class="form-group">
                            <label>Class:</label>
                            <select class="form-control" name="class">
                            @foreach($cabin as $cabins)
                                <option value="{{$cabins->id}} @if ($request->class == $cabins->id) selected @endif">{{$cabins->cabin}}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                    
                    <div class="col-md-3 col-lg-2">
                        <div class="form-group">
                            <label>Person:</label>
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default btn-number btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="person">
                                        <span class="glyphicon glyphicon-minus"></span>
                                    </button>
                                </span>
                                <input type="text" name="person" class="form-control input-number search-no" value="{{$request->person}}" min="1" max="10">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="person">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2 col-lg-1 search_box">
                        <div class="form-group" style="margin-top:4px;">
                            <label></label>
                            {{-- <input type="submit" class="btn btn-xs search_btn" name="search_btn" value="Search"> --}}
                            <button type="submit" class="btn btn-sm search_btn">Search</button>
                        </div>
                        
                    </div>


                </div>

            </div>
        {{Form::close()}}
    </div>
</div>


<div class="row" style="margin-top: 20px">
    <div class="col-md-12">
        <div class="portlet light" id="form_wizard_1">
            <div class="portlet-body form">
                <form class="form-horizontal" action="{{ url('reservation/reserve') }}" id="submit_form" method="POST">
                {{csrf_field()}}
                    <div class="form-wizard">
                        <div class="form-body">
                            <ul class="nav nav-pills nav-justified steps">
                                <li>
                                    <a href="#tab1" data-toggle="tab" class="step">
                                        <span class="number"> 1 </span>
                                        <span class="desc">
                                            <i class="fa fa-check"></i> Find </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab2" data-toggle="tab" class="step">
                                        <span class="number"> 2 </span>
                                        <span class="desc">
                                            <i class="fa fa-check"></i> Detail </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab3" data-toggle="tab" class="step active">
                                        <span class="number"> 3 </span>
                                        <span class="desc">
                                            <i class="fa fa-check"></i> Book </span>
                                    </a>
                                </li>
                               
                            </ul>
                            <div id="bar" class="progress progress-striped" role="progressbar">
                                <div class="progress-bar progress-bar-success"> </div>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab1">
                                    <h4>Departure Flight: {{$request->source}} To {{$request->destination}}</h4>
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="tableSelect">
                                        <thead>
                                            <tr>
                                                <th>S.N.</th>
                                                <th> Flight </th>
                                                <th> Departure/Arrival</th>
                                                <th> Price </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($scheduler as $key => $details)
                                            {{-- sent scheuler id --}}
                                            <input type="hidden" name="scheduler_id[0]" value="{{$details->id}}">
                                            <tr class="odd gradeX">
                                                <td>{{++$key}}</td>
                                                <td>
                                                    {{$details->flight->flight_no}}
                                                </td>
                                                <td>
                                                    <div class="col-md-12">
                                                        <h4>Himalaya Air</h4>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <h5 class="col-md-12">{{$details->source}}</h5>
                                                        <span class="col-md-7 flight-time">{{$details->arrive_time}}</span>
                                                        <div class="col-md-5 flight-time-padding">
                                                            {{-- <span class="time-font">AM</span> <br> --}}
                                                            <span class="time-font">{{date('M d Y', strtotime($details->date))}}</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">To</div>
                                                        <div class="row">
                                                            <hr class="col-md-6 hr-black">
                                                            <div class="col-md-3">
                                                                <i class="fa fa-plane icon-margin"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                         <h5 class="col-md-12">{{$details->destination}}</h5>
                                                        <span class="col-md-7 flight-time">{{$details->depart_time}}</span>
                                                        <div class="col-md-5 flight-time-padding">
                                                            {{-- <span class="time-font">AM</span> <br> --}}
                                                            <span class="time-font">{{date('M d Y', strtotime($details->date_depart))}}</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                   
                                                    @if($currency->billing_currency == "NPR" && $request->trip == "one")
                                                    <label for="cost-check">
                                                        Rs{{$details->price_local_oneway}}
                                                    </label>
                                                    <input type="radio" name="price_radio" value="{{$details->price_local_oneway}}" checked="">
                                                    @elseif($currency->billing_currency == "USD" && $request->trip == "one")
                                                    <label for="cost-check">
                                                        USD {{$details->price_usd_oneway}}
                                                    </label>
                                                    <input type="radio" name="price_radio" value="{{$details->price_usd_oneway}}" checked="">
                                                    @elseif($currency->billing_currency == "NPR" && $request->trip == "round")
                                                    <label for="cost-check">
                                                        Rs {{$details->price_local_roundtrip/2}}
                                                    </label>
                                                    <input type="radio" name="price_radio" value="{{$details->price_local_roundtrip/2}}" checked="">
                                                    @elseif($currency->billing_currency == "USD" && $request->trip == "round")
                                                    <label for="cost-check">
                                                        USD {{$details->price_usd_roundtrip/2}}
                                                    </label>
                                                    <input type="radio" name="price_radio" value="{{$details->price_usd_roundtrip/2}}" checked="">
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <hr>
                                    @if (count($arrival) > 0)
                                        <h4>Returning Flight: {{$request->destination}} To {{$request->source}}</h4>
                                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="tableSelect2">
                                            <thead>
                                                <tr>
                                                    <th>S.N.</th>
                                                    <th> Flight </th>
                                                    <th> Departure/Arrival</th>
                                                    <th> Price </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($arrival as $key => $details)
                                                 {{-- sent scheuler id --}}
                                            <input type="hidden" name="scheduler_id[1]" value="{{$details->id}}">
                                                <tr class="odd gradeX">
                                                    <td>{{++$key}}</td>
                                                    <td>
                                                        {{$details->flight->flight_no}}
                                                    </td>
                                                    <td>
                                                        <div class="col-md-12">
                                                            <h4>Himalaya Air</h4>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <h5 class="col-md-12">{{$details->source}}</h5>
                                                            <span class="col-md-7 flight-time">{{$details->arrive_time}}</span>
                                                            <div class="col-md-5 flight-time-padding">
                                                                {{-- <span class="time-font">AM</span> <br> --}}
                                                                <span class="time-font">{{date('M d Y', strtotime($details->date))}}</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="row">To</div>
                                                            <div class="row">
                                                                <hr class="col-md-6 hr-black">
                                                                <div class="col-md-3">
                                                                    <i class="fa fa-plane icon-margin"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <h5 class="col-md-12">{{$details->destination}}</h5>
                                                            <span class="col-md-7 flight-time">{{$details->depart_time}}</span>
                                                            <div class="col-md-5 flight-time-padding">
                                                                <span class="time-font">{{date('M d Y', strtotime($details->date_depart))}}</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="checkradio">
                                                        @if($currency->billing_currency == "NPR" 
                                                            && $request->trip == "one")
                                                            <label for="cost-check">
                                                                Rs {{$details->price_local_oneway}}
                                                            </label>
                                                            <input type="radio" name="price_radio1" value="{{$details->price_local_oneway}}" checked="">
                                                        @elseif($currency->billing_currency == "USD" 
                                                            && $request->trip == "one")
                                                            <label for="cost-check">
                                                                USD {{$details->price_usd_oneway}}
                                                            </label>
                                                            <input type="radio" name="price_radio1" value="{{$details->price_usd_oneway}}" checked="">
                                                        @elseif($currency->billing_currency == "NPR" 
                                                            && $request->trip == "round")
                                                            <label for="cost-check">
                                                                Rs{{$details->price_local_roundtrip/2}}
                                                            </label>
                                                            <input type="radio" name="price_radio1" value="{{$details->price_local_roundtrip/2}}" checked="">
                                                        @elseif($currency->billing_currency == "USD" 
                                                            && $request->trip == "round")
                                                            <label for="cost-check">
                                                                USD {{$details->price_usd_roundtrip/2}}
                                                            </label>
                                                            <input type="radio" name="price_radio1" value="{{$details->price_usd_roundtrip/2}}" checked="">
                                                        @endif
                                                        
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                    <div class="row">
                                        <button type="submit" class="btn book-btn button-next">Book Now</button>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab2">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn book-btn back-btn pull-right button-previous">Back</button>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5 class="col-md-4"><strong>From:</strong></h5>
                                            <h5 class="col-md-8">{{$request->source}}</h5>
                                            <h5 class="col-md-4"><strong>To:</strong></h5>
                                            <h5 class="col-md-6">{{$request->destination}}</h5>
                                            <h5 class="col-md-4"><strong>Departure:</strong></h5>
                                            <h5 class="col-md-6">{{date('M d Y', strtotime($request->fromDate))}} {{date('D', strtotime($request->fromDate))}}</h5>
                                        </div>
                                        {{-- if round trip show the returning information --}}
                                        @if (count($arrival) > 0)
                                            <div class="col-md-6">
                                                <h5 class="col-md-4"><strong>From:</strong></h5>
                                                <h5 class="col-md-8">{{$request->destination}}</h5>
                                                <h5 class="col-md-4"><strong>To:</strong></h5>
                                                <h5 class="col-md-8">{{$request->source}}</h5>
                                                <h5 class="col-md-4"><strong>Return:</strong></h5>
                                                <h5 class="col-md-8">{{date('M d Y', strtotime($request->toDate))}} {{date('D', strtotime($request->toDate))}}</h4>
                                            </div>
                                        @endif
                                        <div class="col-md-12">
                                            <h5 class="col-md-2"><strong>Class:</strong></h5>
                                            <h5 class="col-md-6">{{$class->getCabin($request->class)}}</h4>
                                        </div>
                                    </div>

                                    @if($request->person > 0)
                                        @for($i=1; $i <= $request->person; $i++)
                                          <hr>
                                          {{-- have backup --}}
                                            {{-- <div class="row"> --}}
                                                <div class="col-md-12">
                                                    <h3><strong>Customer Details {{$i}}</strong></h3>
                                                    {{-- loop till number of customers --}}
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Full Name:</label>
                                                        <div class="col-md-3">
                                                            <input type="text" name="form[{{$i}}][fname]" class="form-control input-small fname" placeholder="First Name" autocomplete="off" onkeydown="upperCaseF(this)">
                                                        </div>


                                                        <div class="col-md-3">
                                                            <input type="text" name="form[{{$i}}][mname]" class="form-control input-small" placeholder="Middle Name" autocomplete="off" onkeydown="upperCaseF(this)">
                                                        </div>
                                                        <div class="col-md-3">
                                                            <input type="text" name="form[{{$i}}][lname]" class="form-control input-small fname" placeholder="Last Name" autocomplete="off" onkeydown="upperCaseF(this)">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label"> Nationality:</label>
                                                        <div class="col-md-3">
                                                            <select name="form[{{$i}}][nationality_id]" class="form-control nationality">
                                                                @foreach(App\   Nationality::pluck('name', 'id') as $key => $value)
                                                                    <option value="{{$key}}">{{$value}}</option>                
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Date of Birth : </label>
                                                        <div class="col-md-3">
                                                            <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
                                                                <input type="text" class="form-control dob" name="form[{{$i}}][dob]">
                                                                <span class="input-group-btn">
                                                                    <button class="btn default" type="button">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </button>
                                                                </span>
                                                            </div>
                                                            <span class="msg-class"></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Passport No.:</label>
                                                        <div class="col-md-6">
                                                            <input type="text" name="form[{{$i}}][passport]" class="form-control passport" placeholder="Passport Number" autocomplete="off" onkeydown="upperCaseF(this)">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Issued Date:</label>
                                                        <div class="col-md-3">

                                                            <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" id="date-picker1"  >

                                                                <input type="text" class="form-control issued_date" readonly="" name="form[{{$i}}][issued_date]">
                                                                <span class="input-group-btn">
                                                                    <button class="btn default" type="button">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <label class="col-md-2 control-label">Expiry Date:</label>
                                                        <div class="col-md-3">
                                                            <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-start-date="+1d">
                                                                <input type="text" class="form-control exp_date" readonly="" name="form[{{$i}}][expiry_date]" >
                                                                <span class="input-group-btn">
                                                                    <button class="btn default" type="button">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Meal Preference</label>
                                                        <div class="col-md-3">
                                                            <select class="form-control meal" name="form[{{$i}}][meal_id]">
                                                                <option selected disabled>Select your Meal Preference</option>
                                                                @foreach(App\Meal::pluck('meal_name', 'id') as $key => $value)
                                                                    <option value="{{$key}}">{{$value}}</option>                
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Seat Preference</label>
                                                        <div class="col-md-3">
                                                            <select class="form-control seat" name="form[{{$i}}][seat_id]">
                                                                <option selected disabled>Select your Seat Preference</option>
                                                                @foreach(App\Seat::pluck('seat_name', 'id') as $key => $value)
                                                                    <option value="{{$key}}">{{$value}}</option>                
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Mobile No.:</label>
                                                        <div class="col-md-3">
                                                            <input type="text" name="form[{{$i}}][mobile_no]" class="form-control mobile" placeholder="Mobile Number" autocomplete="off" onkeypress="return isNumber(event)">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">E-mail:</label>
                                                        <div class="col-md-6">
                                                            <input type="email" name="form[{{$i}}][email]" class="form-control email" placeholder="E-mail">
                                                        </div>
                                                    </div>
                                                </div>
                                            {{-- </div> --}}
                                            {{-- have backup upto here --}}
                                        @endfor
                                         <hr>
                                        <button class="btn book-btn book-btn-info button-next" id="passenger_detail">Next</button>

                                        <div id="hidden">
                                            <input type="hidden" data-person="{{$request->person}}" id="no_person">
                                            <input type="hidden" data-flight="{{$details->flight->flight_no}}" id="no_flight">
                                        </div>

                                    @endif
                                </div>
                                <div class="tab-pane" id="tab3">
                                    <div class="row">
                                        <div class="col-md-12">
                                        {{-- for single trip tab3 --}}
                                            <table class="table table-striped table-hover">
                                                <thead><span style="font-size:20px;font-weight: bold">Leaving Flight</span></thead>
                                                <tbody id="tbody">
                                                    <tr>
                                                        <td> Flight No.  </td>
                                                        <td>  </td>
                                                        <td>  </td>
                                                        <td style="text-align: right;" id="flight"> </td>
                                                    </tr>

                                                    <tr>
                                                        <td> No. of Passengers </td>
                                                        <td>  </td>
                                                        <td>  </td>
                                                        <td style="text-align: right;" id="passengers"> </td>
                                                    </tr>

                                                    <tr id="trFare">
                                                        <td> Fare </td>
                                                        <td>  </td>
                                                        <td>  </td>
                                                        <td style="text-align: right;" id="fare"></td>
                                                    </tr>

                                                    {{-- <tr>
                                                        <td>Discount</td>
                                                        <td>  </td>
                                                        <td>  </td>
                                                        <td style="text-align: right;"> 219.00</td>
                                                    </tr> --}}

                                                    <tr>
                                                        <td >Total fare Inbound flight</td>
                                                        <td>  </td>
                                                        <td>  </td>
                                                        <td style="text-align: right;"><strong id="total"></strong></td>
                                                    </tr>
                                                </tbody>
                                            </table>


                                            {{--For round trip tab3  --}}
                                            @if (count($arrival) > 0)
                                            <table class="table table-striped table-hover">
                                            <thead><span style="font-size:20px;font-weight: bold">Returning Flight</span></thead>
                                                <tbody id="tbody1">
                                                    <tr>
                                                        <td> Flight No.  </td>
                                                        <td>  </td>
                                                        <td>  </td>
                                                        <td style="text-align: right;" id="flight1"> </td>
                                                    </tr>

                                                    <tr>
                                                        <td> No. of Passengers </td>
                                                        <td>  </td>
                                                        <td>  </td>
                                                        <td style="text-align: right;" id="passengers1"> </td>
                                                    </tr>

                                                    <tr id="trFare1">
                                                        <td> Fare </td>
                                                        <td>  </td>
                                                        <td>  </td>
                                                        <td style="text-align: right;" id="fare1"></td>
                                                    </tr>

                                                    <tr>
                                                        <td >Total fare Inbound flight</td>
                                                        <td>  </td>
                                                        <td>  </td>
                                                        <td style="text-align: right;"><strong id="total1"></strong></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            @endif


                                        </div>
                                    </div>
                                    <button class="btn btn-info" id="save_and_print">Book and Print Ticket</button>
                                    <button class="btn btn-info" id="save_and_back">Book and go back</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection


@section('footerscripts')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="../assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{ url('js/reserve_form.js') }}" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
      $('#tableSelect tbody tr').click(function(){
        // console.log('clicked');
        if (($('#tableSelect tbody tr').find('td.checkradio').find('span.checked')).length > 0) {
          console.log('if here');
          $('#tableSelect tbody tr').find('td.checkradio').find('span.checked').removeClass('checked');
          $(this).find('td.checkradio').find('span').addClass('checked');
        }
        else {
          // console.log('else here');
          $(this).find('td.checkradio').find('span').addClass('checked');
        }
      });

      $('#tableSelect2 tbody tr').click(function(){
        // console.log('clicked');
        if (($('#tableSelect2 tbody tr').find('td.checkradio').find('span.checked')).length > 0) {
          // console.log('if here');
          $('#tableSelect2 tbody tr').find('td.checkradio').find('span.checked').removeClass('checked');
          $(this).find('td.checkradio').find('span').addClass('checked');
        }
        else {
          // console.log('else here');
          $(this).find('td.checkradio').find('span').addClass('checked');
        }
      });

      $(document).ready(function(){
            var city = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('city_name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                    url: '/api/agent/city?city_name=%QUERY',
                    wildcard: '%QUERY'
                    }
            });

            $('#typeaheadSource').typeahead(null, {
                                                  name: 'source',
                                                  display: 'source',
                                                  source: city
                                              });
      });


      var cityDestination = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('city_name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
          url: '/api/agent/cityDestination?city_name=%QUERY',
          wildcard: '%QUERY'
        }
      });

      $('.typeaheadDestination').typeahead(null, {
      name: 'destination',
      display: 'destination',
      source: cityDestination
      });

        function upperCaseF(a){
            setTimeout(function(){
                a.value = a.value.toUpperCase();
            }, 1);
        }

        function isNumber(evt) {
          evt = (evt) ? evt : window.event;
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode > 31 && (charCode < 48 || charCode > 57 )|| (charCode >= 96 && charCode <= 105)||(charCode == 107)){
            if (charCode == 43) {
              return true;
            }
            return false;
          }
          return true;
        }

    //$(document).ready(function(){
        var passengers=$("#no_person").data("person");
        $("#passengers").text(passengers);

        var flightName=$("#no_flight").data("flight");
        $("#flight").text(flightName);

        var click_count=0;

        $("#passenger_detail").on("click",function(){
            click_count++;//so that ajax request doesnt run more than once...else too many redundant data in ajax

            var each_fare = $("#tableSelect input[type='radio'][name='price_radio']:checked").val();
            var total_fare=each_fare*passengers;
            $("#fare").text(total_fare);
            
            if($("#tableSelect2 input[type='radio'][name='price_radio1']:checked").val()!= null){
                var each_fare_round=$("#tableSelect2 input[type='radio'][name='price_radio1']:checked").val();
                var total_fare_round=each_fare_round*passengers;
                $("#fare1").text(total_fare_round);
                $("#passengers1").text(passengers);
                $("#flight1").text(flightName);
            }

            if(click_count == 1){

                var xhr=$.ajax({
                            url: '/tax/gettax',
                            type: 'GET',
                            data: $(this).serialize(),
                            dataType:'json',
                            success: function(response)
                            {
                                //console.log(response);
                                var total_tax=0;
                                var total_tax_round=0;
                                $.each(response, function (key, value) {
                                    if("{{$currency->billing_currency}}" == "NPR"){
                                        $("#trFare").after("<tr><td>"+response[key].tax_head+"</td><td></td><td></td><td style='text-align: right;'>"+(total_fare*response[key].local_price)+"</td></tr>");

                                         if({{count($arrival)}} > 0){
                                            $("#trFare1").after("<tr><td>"+response[key].tax_head+"</td><td></td><td></td><td style='text-align: right;'>"+(total_fare_round*response[key].local_price)+"</td></tr>");
                                            console.log("each_round:"+response[key].tax_head+":"+(response[key].local_price*each_fare));
                                         }

                                        //var tax[key]=response[key].tax_head+":"+(response[key].local_price*each_fare);
                                        console.log("each"+response[key].tax_head+":"+(response[key].local_price*each_fare));
                                        total_tax += total_fare*response[key].local_price;
                                        total_tax_round += total_fare_round*response[key].local_price;

                                    }
                                    else{
                                        $("#trFare").after("<tr><td>"+response[key].tax_head+"</td><td></td><td></td><td style='text-align: right;'>"+(total_fare*response[key].usd_price)+"</td></tr>");

                                        if({{count($arrival)}} > 0){
                                            $("#trFare1").after("<tr><td>"+response[key].tax_head+"</td><td></td><td></td><td style='text-align: right;'>"+(total_fare_round*response[key].usd_price)+"</td></tr>");
                                            console.log("each_round"+response[key].tax_head+":"+(response[key].usd_price*each_fare));
                                         }

                                        console.log("each"+response[key].tax_head+":"+(response[key].usd_price*each_fare));
                                        total_tax += total_fare*response[key].usd_price;
                                        total_tax_round += total_fare_round*response[key].usd_price;
                                    }

                                });
                                //console.log(total_tax);
                                $('<input>').attr({
                                    type: 'hidden',
                                    value: total_tax,
                                    name: 'total_tax'
                                }).appendTo('#hidden');

                                $('<input>').attr({
                                    type: 'hidden',
                                    value: total_tax_round,
                                    name: 'total_tax_round'
                                }).appendTo('#hidden');

                                $('<input>').attr({
                                    type: 'hidden',
                                    value: total_fare,
                                    name: 'total_fare'
                                }).appendTo('#hidden');

                                $('<input>').attr({
                                    type: 'hidden',
                                    value: total_fare_round,
                                    name: 'total_fare_round'
                                }).appendTo('#hidden');

                                $("#total").text("{{$currency->billing_currency}}"+" "+(total_tax+total_fare));
                                $("#total1").text("{{$currency->billing_currency}}"+" "+(total_tax_round+total_fare_round));
                            }
                });    
            }
        });
    //});

    /*function getDate(){
        console.log($("#date-picker1").datepicker("getDate"));
    }*/

    </script>

@endsection



