<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Report</title>
    {{ Html::style('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}
    
    <style>
        .invoice-title h2, .invoice-title h3 {
            display: inline-block;
        }
        .flight-details{
            margin-bottom: 20px;
        }
        .table > tbody > tr > .no-line {
            border-top: none;
        }

        .table > thead > tr > .no-line {
            border-bottom: none;
        }

        .table > tbody > tr > .thick-line {
            border-top: 2px solid;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
        	<div class="invoice-title">
    			<h2>Invoice</h2><h3 class="pull-right">PNR # : <span>12345</span></h3>
    		</div>
            <div>
                <span><strong>Bill Date:</strong></span>
                <span><strong>01/01/2017</span></strong>
            </div>
    		<hr>
    		<div class="row">
    			<div class="col-xs-6">
    				<address>
    				<h4><strong>Billed To:</strong></h4>
    					John Smith<br>
    					1234 Organisation Name<br>
    					Kathmandu,Nepal
    				</address>
    			</div>
    			<div class="col-xs-6 text-right">
    				<address>
        			<h4><strong>Shipped To:</strong></h4>
    					Sampresh Shrestha<br>
    					1234 Main<br>
    					Palpa,Nepal
    				</address>
    			</div>
    		</div>
    	</div>
    </div>

    <div class="row flight-details">
        <div class="col-xs-6">
            <h4><strong>Flight Details:</strong></h4>
            <span>Flight Number:</span>
            <span><strong>Himalaya Airlines - SQ 5311</span></strong></br>
            <span>AirCraft:</span>
            <span><strong>Airbus Industrie A320-100/200</span></strong></br>
            <span>Departure:</span>
            <span><strong>KTM</strong></span>-<span><strong>03/01/2017</strong></span>-<span><strong>01:55 PM</strong></span></br>
            <span>Arrival:</span>
            <span><strong>DHL</strong></span>-<span><strong>03/02/2017</strong></span>-<span><strong>01:00 PM</strong></span></br>
        </div>
        <div class="col-xs-6 text-right">
            <address>
                <h4><strong>Flight Date:</strong></h4>
                March 7, 2014<br><br>
            </address>
        </div>
    </div>
    
    <div class="row">
    	<div class="col-md-12">
    		<div class="panel panel-default">
    			<div class="panel-heading">
    				<h3 class="panel-title"><strong>Invoice summary</strong></h3>
    			</div>
    			<div class="panel-body">
    				<div class="table-responsive">
    					<table class="table table-condensed">
    						<thead>
                                <tr>
        							<td><strong>Ticket No. </strong></td>
        							<td class="text-center"><strong>Ticket Price</strong></td>
                                    {{-- <td class="text-center"><strong>Included Tax Type</strong></td> --}}
        							<td class="text-center"><strong>Included Tax Price</strong></td>
        							<td class="text-right"><strong>Totals</strong></td>
                                </tr>
    						</thead>
    						<tbody>
    							<!-- foreach ($order->lineItems as $line) or some such thing here -->
    							<tr>
    								<td>#A11</td>
    								<td class="text-center"><span>$</span><span>20.00</span></td>
                                    {{-- <td class="text-center"><span>VAT</span>(<span>13%</span>)</td> --}}
    								<td class="text-center"><span>VAT</span>(<span>13%</span>)=<span>$</span><span>2.00</span></td>
    								<td class="text-right"><span>$</span><span>220.00</span></td>
    							</tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    {{-- <td class="text-center"><span>VAT</span>(<span>13%</span>)</td> --}}
                                    <td class="text-center"><span>Service Charge</span>(<span>5%</span>)=<span>$</span><span>0.75</span></td>
                                    <td></td>
                                </tr>


                                <tr>
        							<td>#A12</td>
    								<td class="text-center"><span>$</span><span>20.00</span></td>
    								<td class="text-center"><span>VAT</span>(<span>13%</span>)=<span>$</span><span>2.00</span></td>
    								<td class="text-right"><span>$</span><span>220.00</span></td>
    							</tr>
                                <tr>
            						<td>#A13</td>
    								<td class="text-center"><span>$</span><span>20.00</span></td>
    								<td class="text-center"><span>VAT</span>(<span>13%</span>)=<span>$</span><span>2.00</span></td>
    								<td class="text-right"><span>$</span><span>220.00</span></td>
    							</tr>
    							<tr>
    								<td class="thick-line"></td>
    								<td class="thick-line text-center"><strong>Subtotal T. Price:</strong><span>$440</span></td>
    								<td class="thick-line text-center"><strong>Subtotal Tax:</strong><span>$40</span></td>
    								<td class="thick-line text-right"><strong>Total:</strong><span>$670.99</span></td>
    							</tr>
    							{{-- <tr>
    								<td class="no-line"></td>
    								<td class="no-line"></td>
    								<td class="no-line text-center"><strong>Shipping</strong></td>
    								<td class="no-line text-right">$15</td>
    							</tr>
    							<tr>
    								<td class="no-line"></td>
    								<td class="no-line"></td>
    								<td class="no-line text-center"><strong>Total</strong></td>
    								<td class="no-line text-right">$685.99</td>
    							</tr> --}}
    						</tbody>
    					</table>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>

    <div class="row">
        
    </div>
</div>

</body>