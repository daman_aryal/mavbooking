
@extends('frontend.master')
@section('headerscript')
  {!! Html::style('css/chosen.css') !!}
@endsection
@section('contents')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="index.html">Home</a>
            <i class="fa fa-circle"></i>
        </li>
    </ul>
</div>
<div class="row" style="margin-top: 20px">
    <div class="col-md-12">
        <div class="portlet light bordered">
            {!! Form::open(array('url' => '/reservation/search','method'=>'POST')) !!}
                <div class="form-body">
                    <div class="row form-padding">
                        <div class="col-md-12" style="padding-left: 30px">
                            <div class="form-group">
                                <div class="md-radio-inline">
                                    <div class="md-radio">
                                        <input type="radio" id="radio1" name="trip" class="md-radiobtn" value="one">
                                        <label for="radio1">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> One Way </label>
                                    </div>
                                    <div class="md-radio">
                                        <input type="radio" id="radio2" name="trip" class="md-radiobtn" value="round" checked="checked">
                                        <label for="radio2">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> Round Trip </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        {{-- <div class="form-group">
                                            <input class="form-control input-lg input-resp" type="text" name="origin" placeholder="Select Origin">
                                        </div> --}}

                                        <select data-placeholder="Source" name = "source" class="chosen-select my_select_box" style="width:350px;" tabindex="2">
                                                  <option value=""></option>
                                                  @foreach ($cities as $key => $city)
                                                    <option value="{{ $city->source }}">{{ $city->source }}</option>
                                                  @endforeach

                                        </select>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="col-md-5 date-picker-label" for="date-picker_from">Leaving On</label>
                                            <div class="input-group date date-picker col-md-7" data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
                                                <input type="text" placeholder="Leaving Date" name="fromDate" class="form-control datepicker_input" name="datepicker_from">
                                                <span class="input-group-btn">
                                                    <button class="btn default datepicker_btn" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        {{-- <div class="form-group">
                                            <input class="form-control input-lg input-resp" name="destination" type="text" placeholder="Select Destination">
                                        </div> --}}
                                        <select data-placeholder="Destination" name="destination" class="chosen-select my_select_box" style="width:350px;" tabindex="2">
                                                  <option value=""></option>
                                                  @foreach ($cities as $key => $city)
                                                    <option value="{{ $city->destination }}"> {{ $city->destination }} </option>
                                                  @endforeach

                                        </select>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group" id="not-for-one">
                                            <label class="col-md-5 date-picker-label" for="date-picker_from">Returning On</label>
                                            <div class="input-group date date-picker col-md-7" data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
                                                <input type="text" name="toDate" placeholder="Returning Date" class="form-control datepicker_input" name="datepicker_to">
                                                <span class="input-group-btn">
                                                    <button class="btn default datepicker_btn" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-md-4 class-label">Class:</label>
                                            <div class="col-md-8 input-group">
                                                <select name="class" class="form-control input-lg input-resp" style="height:34px;padding:4px 16px;font-size:16px;">
                                                    @foreach($cabin as $cabins)
                                                        <option value="{{$cabins->id}}">{{$cabins->cabin}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="col-md-4 class-label">Person:</label>
                                            <div class="input-group">
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-default btn-number inc-dec-btn" disabled="disabled" data-type="minus" data-field="person">
                                                        <span class="glyphicon glyphicon-minus"></span>
                                                    </button>
                                                </span>
                                                <input type="text" name="person" class="form-control input-number" value="1" min="1" max="10">
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-default btn-number inc-dec-btn" data-type="plus" data-field="person">
                                                        <span class="glyphicon glyphicon-plus"></span>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                             <div class="form-group">
                                <button type="submit" class="btn search_btn home_search"><i class="fa fa-search"></i> Search
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-sm-6">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bar-chart font-green"></i>
                    <span class="caption-subject font-green bold uppercase">Flight Stats</span>
                    <span class="caption-helper">flight stats...</span>
                </div>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
    <div class="col-md-6 col-sm-6">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bar-chart font-green"></i>
                    <span class="caption-subject font-green bold uppercase">Sales By Region</span>
                    <span class="caption-helper">distance stats...</span>
                </div>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>
@endsection

@section('footerscripts')
  {!! Html::script('js/chosen.jquery.min.js') !!}
  <script>
    $(document).ready(function (e){
      $(".my_select_box").chosen({
        disable_search_threshold: 10,
        no_results_text: "Oops, nothing found!",
        width: "100%"
      });
    });
  </script>
@endsection